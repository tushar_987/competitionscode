//package hackerearth_sept;


import com.sun.org.apache.xerces.internal.parsers.IntegratedParserConfiguration;
import java.io.*;
import java.util.*;
import java.math.*;

/**
 *
 * @author Tushar 30 Sep, 2017
 */
class Journey {
    public static void main(String args[])
    {

        //File file= new File("COMPLETE_FILE_PATH");
        //InputStream is = new FileInputStream(file);
        
        
        //InputReader in = new InputReader(is);
        InputReader in = new InputReader(System.in);
        PrintWriter out= new PrintWriter(System.out);
        
        int t=1;
        Task solver=new Task();
        //long start = System.nanoTime();
        solver.solve(t,in,out);
        //long stop = System.nanoTime();
        //System.out.println("Execution time: " + ((stop - start) / 1e+6) + "ms.");
        

        out.flush();
        out.close();


    }

    static class Task {
        long mod = 1000000007;
        int[] l,c,p;
        int n,m;
        Map<Long,HashMap<Integer,Integer>> map;

        public void solve(int testNumber, InputReader in,PrintWriter out){
            n=in.nextInt()-1;m=in.nextInt();
            l=new int[n+1];
            c=new int[n+1];
            p=new int[n+1];
            for(int j=1;j<=n;j++)   l[j]=in.nextInt();
            for(int j=1;j<=n;j++)   c[j]=in.nextInt();
            for(int j=1;j<=n;j++)   p[j]=in.nextInt();
            
            map= new HashMap<>();
            for(int i=0;i<m;i++){
                long init= in.nextLong();
                    out.println(dp(init,1));
                
            }
            
            
            
        }
        
        int dp(long cap,int index){
            if(!map.isEmpty() && map.containsKey(cap)  && map.get(cap).containsKey(index)){
                return map.get(cap).get(index);
            }
            else{
                if(map.isEmpty()) map.put(cap,new HashMap<Integer,Integer>());
                else if(!map.containsKey(cap))  map.put(cap,new HashMap<Integer,Integer>());
                
                int ans;
                if(index==n+1){
                    if(cap<0)   ans=-1;
                    else ans=0;
                    return ans;
                }
                if(index>n+1){
                    ans=0;
                    map.get(cap).put(index, ans);
                    return ans;
                }
                if(cap<0){
                    ans=-1;
                    map.get(cap).put(index, ans);
                    return ans;
                }
                
                int second=dp(c[index]-l[index],index+1);
                
                if(second!=-1)  second=p[index]+second;
                
                if(cap==0) {
                    ans=second;
                    map.get(cap).put(index, ans);
                    return ans;
                }

                int first=dp(cap-l[index],index+1);//Not Included case
                
                if(first==-1 && second==-1) ans=-1;
                else if(first==-1)  ans=second;
                else if(second==-1) ans=first;
                else    ans=Math.min(first, second);
                
                
                map.get(cap).put(index, ans);
                return ans;
            }
        }
            
        int LOG2(long item){
            int count=0;
            while(item>1){
                item>>=1;count++;
            }
            return count;
        }

        long power(long x, long n) {
            if (n <= 0) {
                return 1;
            }
            long y = power(x, n / 2);

            if ((n & 1) == 1) {
                return (((y * y) % mod) * x) % mod;
            }

            return (y * y) % mod;
        }

        long gcd(long a, long b) {
            a = Math.abs(a);
            b = Math.abs(b);
            return BigInteger.valueOf(a).gcd(BigInteger.valueOf(b)).longValue();
        }
    }
    
    
    static class InputReader {
 
        private InputStream stream;
        private byte[] buf = new byte[8192];
        private int curChar, snumChars;
        private SpaceCharFilter filter;
 
        public InputReader(InputStream stream) {
            this.stream = stream;
        }
 
        public int snext() {
            if (snumChars == -1)
                throw new InputMismatchException();
            if (curChar >= snumChars) {
                curChar = 0;
                try {
                    snumChars = stream.read(buf);
                } catch (IOException e) {
                    throw new InputMismatchException();
                }
                if (snumChars <= 0)
                    return -1;
            }
            return buf[curChar++];
        }
 
        public int nextInt() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            int res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public long nextLong() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            long res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public int[] nextIntArray(int n) {
            int a[] = new int[n];
            for (int i = 1; i <n; i++)
                a[i] = nextInt();
            return a;
        }
 
        public String readString() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            StringBuilder res = new StringBuilder();
            do {
                res.appendCodePoint(c);
                c = snext();
            } while (!isSpaceChar(c));
            return res.toString();
        }

        public String next() {
            return readString();
        }
 
        public boolean isSpaceChar(int c) {
            if (filter != null)
                return filter.isSpaceChar(c);
            return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
        }
 
        public interface SpaceCharFilter {
            public boolean isSpaceChar(int ch);
        }
    }


}
