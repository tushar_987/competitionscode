//package hackerearth_sept;


import java.io.*;
import java.util.*;
import java.math.*;

/**
 *
 * @author Tushar 29 Sep, 2017
 */
class K_Ancestor {
    public static void main(String args[])
    {

        //File file= new File("COMPLETE_FILE_PATH");
        //InputStream is = new FileInputStream(file);
        
        
        //InputReader in = new InputReader(is);
        InputReader in = new InputReader(System.in);
        PrintWriter out= new PrintWriter(System.out);
        
        int t=1;
        Task solver=new Task();
        //long start = System.nanoTime();
        solver.solve(t,in,out);
        //long stop = System.nanoTime();
        //System.out.println("Execution time: " + ((stop - start) / 1e+6) + "ms.");
        

        out.flush();
        out.close();


    }

    static class Task {
        long mod = 1000000007;
        int n,k;
        int[] arr,ans; 
        List<List<Integer>> v;
        List<List<Integer>> adj;
        public void solve(int testNumber, InputReader in,PrintWriter out){
            n=in.nextInt();
            k= in.nextInt();
            arr= new int[n+1];
            ans= new int[n+1];
            adj= new ArrayList<>(1000000+7);
            v= new ArrayList<>(1000000+7);
            
            
            for(int i=1;i<=n;i++)   arr[i]=in.nextInt();
            
            for(int i=0;i<=n;i++)    adj.add(new ArrayList<Integer>());
            for(int i=1;i<n;i++){
                int a=in.nextInt();
                int b= in.nextInt();
                adj.get(a).add(b);
                adj.get(b).add(a);
            }
            
            for(int i=0;i<1e6+7;i++)    v.add(new ArrayList<Integer>());
            
            dfs(1,-1);
            
            for(int i=1;i<=n;i++){
                out.print(ans[i]+" ");
            }
            





        }
        
        
        void dfs(int from,int par){
            int size=v.get(arr[from]).size();
            if(size>=k) ans[from]=v.get(arr[from]).get(size-k);
            else    ans[from]=-1;
            
            for(int to:adj.get(from)){
                if(to != par){
                    v.get(arr[from]).add(from);
                    dfs(to,from);
                    v.get(arr[from]).remove(size);
                }
            }
        }
        
            
        int LOG2(long item){
            int count=0;
            while(item>1){
                item>>=1;count++;
            }
            return count;
        }

        long power(long x, long n) {
            if (n <= 0) {
                return 1;
            }
            long y = power(x, n / 2);

            if ((n & 1) == 1) {
                return (((y * y) % mod) * x) % mod;
            }

            return (y * y) % mod;
        }

        long gcd(long a, long b) {
            a = Math.abs(a);
            b = Math.abs(b);
            return BigInteger.valueOf(a).gcd(BigInteger.valueOf(b)).longValue();
        }
    }
    
    
    static class InputReader {
 
        private InputStream stream;
        private byte[] buf = new byte[8192];
        private int curChar, snumChars;
        private SpaceCharFilter filter;
 
        public InputReader(InputStream stream) {
            this.stream = stream;
        }
 
        public int snext() {
            if (snumChars == -1)
                throw new InputMismatchException();
            if (curChar >= snumChars) {
                curChar = 0;
                try {
                    snumChars = stream.read(buf);
                } catch (IOException e) {
                    throw new InputMismatchException();
                }
                if (snumChars <= 0)
                    return -1;
            }
            return buf[curChar++];
        }
 
        public int nextInt() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            int res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public long nextLong() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            long res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public int[] nextIntArray(int n) {
            int a[] = new int[n];
            for (int i = 0; i < n; i++)
                a[i] = nextInt();
            return a;
        }
 
        public String readString() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            StringBuilder res = new StringBuilder();
            do {
                res.appendCodePoint(c);
                c = snext();
            } while (!isSpaceChar(c));
            return res.toString();
        }

        public String next() {
            return readString();
        }
 
        public boolean isSpaceChar(int c) {
            if (filter != null)
                return filter.isSpaceChar(c);
            return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
        }
 
        public interface SpaceCharFilter {
            public boolean isSpaceChar(int ch);
        }
    }


}
