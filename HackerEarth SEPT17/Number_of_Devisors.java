//package hackerearth_sept;


import java.io.*;
import java.util.*;
import java.math.*;

/**
 *
 * @author Tushar 28 Sep, 2017
 */
class Number_of_Devisors {
    public static void main(String args[])
    {

        //File file= new File("COMPLETE_FILE_PATH");
        //InputStream is = new FileInputStream(file);
        
        
        //InputReader in = new InputReader(is);
        InputReader in = new InputReader(System.in);
        PrintWriter out= new PrintWriter(System.out);
        
        int t=1;
        while(t-->0){
        Task solver=new Task();
        //long start = System.nanoTime();
        solver.solve(t,in,out);
        //long stop = System.nanoTime();
        //System.out.println("Execution time: " + ((stop - start) / 1e+6) + "ms.");
        }

        out.flush();
        out.close();


    }

    static class Task {
        int mod = 1000000007;

        public void solve(int testNumber, InputReader in,PrintWriter out){
            int x= in.nextInt();
            int[] arr= new int[x];
            long mul=1;
            
            for(int i=0;i<x;i++){
                int num= in.nextInt()+1;
                arr[i]=num;
                mul=(mul*arr[i])%mod;
            }
            
            long result=1;
            
            for(int i=0;i<x;i++){
                long sig=sigma(arr[i]);
                long tmp= (((mul*invrMod(arr[i],mod))%mod)*sig)%mod;
//                out.print(tmp+1+" ");
                result=(result*(tmp+1))%mod;
            }
//            out.println();
            out.println(result);
            





        }
        long[] extended_euclid(long a, long b){
                if(b==0){
                        long[] arr= new long[3];
                        arr[0]=a;
                        arr[1]=1;
                        arr[2]=0;
                        return arr;
                }
                else{
                        long[] arr= new long[3];
                        long[] arr2=new long[3];
                        arr2=extended_euclid(b,a%b);
                        arr[0]=arr2[0];
                        arr[1]=arr2[2];
                        long xx=arr2[1];
                        arr[2]=xx-(long)(Math.floor(a/b)*arr2[2]);
                        return arr;
                }

        }
        
        
        long invrMod(long n,long M)
        {
            long[] vr=new long[3];
            vr=extended_euclid(n,M);
            long x=(vr[1]+M)%M;
            return x;
        }
        
        long sigma(long num){
            if(num%2==0){
                return ((num/2)*(num-1))%mod;
            }
            else{
                return (((num-1)/2)*num)%mod;
            }
        }
            
        int LOG2(long item){
            int count=0;
            while(item>1){
                item>>=1;count++;
            }
            return count;
        }

        long power(long x, long n) {
            if (n <= 0) {
                return 1;
            }
            long y = power(x, n / 2);

            if ((n & 1) == 1) {
                return (((y * y) % mod) * x) % mod;
            }

            return (y * y) % mod;
        }

        long gcd(long a, long b) {
            a = Math.abs(a);
            b = Math.abs(b);
            return BigInteger.valueOf(a).gcd(BigInteger.valueOf(b)).longValue();
        }
    }
    
    
    static class InputReader {
 
        private InputStream stream;
        private byte[] buf = new byte[8192];
        private int curChar, snumChars;
        private SpaceCharFilter filter;
 
        public InputReader(InputStream stream) {
            this.stream = stream;
        }
 
        public int snext() {
            if (snumChars == -1)
                throw new InputMismatchException();
            if (curChar >= snumChars) {
                curChar = 0;
                try {
                    snumChars = stream.read(buf);
                } catch (IOException e) {
                    throw new InputMismatchException();
                }
                if (snumChars <= 0)
                    return -1;
            }
            return buf[curChar++];
        }
 
        public int nextInt() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            int res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public long nextLong() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            long res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public int[] nextIntArray(int n) {
            int a[] = new int[n];
            for (int i = 0; i < n; i++)
                a[i] = nextInt();
            return a;
        }
 
        public String readString() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            StringBuilder res = new StringBuilder();
            do {
                res.appendCodePoint(c);
                c = snext();
            } while (!isSpaceChar(c));
            return res.toString();
        }

        public String next() {
            return readString();
        }
 
        public boolean isSpaceChar(int c) {
            if (filter != null)
                return filter.isSpaceChar(c);
            return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
        }
 
        public interface SpaceCharFilter {
            public boolean isSpaceChar(int ch);
        }
    }


}
