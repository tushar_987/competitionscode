//package codeagon;


import java.io.*;
import java.util.*;
import java.math.*;

/**
 *
 * @author Tushar 2 Sep, 2017
 */
class C {
    public static void main(String args[])
    {

        //File file= new File("COMPLETE_FILE_PATH");
        //InputStream is = new FileInputStream(file);
        
        
        //InputReader in = new InputReader(is);
        InputReader in = new InputReader(System.in);
        PrintWriter out= new PrintWriter(System.out);
        
        int t=1;
        Task solver=new Task();
        //long start = System.nanoTime();
        solver.solve(t,in,out);
        //long stop = System.nanoTime();
        //System.out.println("Execution time: " + ((stop - start) / 1e+6) + "ms.");
        

        out.flush();
        out.close();


    }

    static class Task {
        long mod = 1000000007;

        public void solve(int testNumber, InputReader in,PrintWriter out){
            int n=in.nextInt();
            int m=in.nextInt();
            int k=in.nextInt();
            int[] arr= new int[n];
            
            int last_one=Integer.MIN_VALUE;
            int first_one=Integer.MAX_VALUE;
            int count_one=0;
            
            for(int i=0;i<n;i++){
                arr[i]=in.nextInt();
                if(arr[i]==1){
                    last_one=i;
                    if(i<first_one) first_one=i;
                    count_one++;
                }
            }
            
            if(count_one<m) {
                out.println(-1);
                return;
            }
            
            long sum1=0;
            int prev=last_one;
            int c=m;
            if(last_one>=0)c--;
            for(int index=last_one-1;index>=0;index--){
                if(arr[index]==1){
                    int add=prev-index;
                    
                    if(c>0){
                        sum1+=(long)add*(long)c*(long)k;
                        c--;
                        prev=index;
                    }
                }
                if(c==0){
                    sum1+=prev;
                    index=-1;
                }
            }
            
            
            c=0;
            prev=0;
            long sum2=0;
            for(int index=first_one;index<n;index++){
                if(arr[index]==1){
                    int add= index-prev;
                    if(c==0)    sum2+=add;
                    else    sum2+=(long)add*(long)c*(long)k;
                    
                    c++;
                    prev=index;
                }
                if(c==m){
                    index=n;
                }
            }
            
            
            out.println(Math.min(sum1, sum2));
            





        }
            
        int LOG2(long item){
            int count=0;
            while(item>1){
                item>>=1;count++;
            }
            return count;
        }

        long power(long x, long n) {
            if (n <= 0) {
                return 1;
            }
            long y = power(x, n / 2);

            if ((n & 1) == 1) {
                return (((y * y) % mod) * x) % mod;
            }

            return (y * y) % mod;
        }

        long gcd(long a, long b) {
            a = Math.abs(a);
            b = Math.abs(b);
            return BigInteger.valueOf(a).gcd(BigInteger.valueOf(b)).longValue();
        }
    }
    
    
    static class InputReader {
 
        private InputStream stream;
        private byte[] buf = new byte[8192];
        private int curChar, snumChars;
        private SpaceCharFilter filter;
 
        public InputReader(InputStream stream) {
            this.stream = stream;
        }
 
        public int snext() {
            if (snumChars == -1)
                throw new InputMismatchException();
            if (curChar >= snumChars) {
                curChar = 0;
                try {
                    snumChars = stream.read(buf);
                } catch (IOException e) {
                    throw new InputMismatchException();
                }
                if (snumChars <= 0)
                    return -1;
            }
            return buf[curChar++];
        }
 
        public int nextInt() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            int res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public long nextLong() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            long res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public int[] nextIntArray(int n) {
            int a[] = new int[n];
            for (int i = 0; i < n; i++)
                a[i] = nextInt();
            return a;
        }
 
        public String readString() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            StringBuilder res = new StringBuilder();
            do {
                res.appendCodePoint(c);
                c = snext();
            } while (!isSpaceChar(c));
            return res.toString();
        }

        public String next() {
            return readString();
        }
 
        public boolean isSpaceChar(int c) {
            if (filter != null)
                return filter.isSpaceChar(c);
            return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
        }
 
        public interface SpaceCharFilter {
            public boolean isSpaceChar(int ch);
        }
    }


}
