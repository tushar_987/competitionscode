//package practice;

import java.io.*;
import java.util.*;
import java.math.*;

/**
 *
 * @author Tushar 29 Dec, 2017
 */
public class B {

    public static void main(String args[]) {

        //File file= new File("COMPLETE_FILE_PATH");
        //InputStream is = new FileInputStream(file);
        //InputReader in = new InputReader(is);
        InputReader in = new InputReader(System.in);
        PrintWriter out = new PrintWriter(System.out);

        int t = 1;
        Task solver = new Task();
        //long start = System.nanoTime();
        solver.solve(t, in, out);
        //long stop = System.nanoTime();
        //System.out.println("Execution time: " + ((stop - start) / 1e+6) + "ms.");

        out.flush();
        out.close();

    }

    static class Task {

        long mod = 1000000007;

        public void solve(int testNumber, InputReader in, PrintWriter out) {
            int n = in.nextInt();
            int m = in.nextInt();

            char[][] mat = new char[n+2][m+2];
            position start=new position(0, 0);
            position end=new position(0, 0);
            for(int i=0;i<m+2;i++) {
                mat[0][i]='*';
                mat[n+1][i]='*';
            }
            for(int i=0;i<n+2;i++) {
                mat[i][0]='*';
                mat[i][m+1]='*';
            }
            for (int i = 1; i < n+1; i++) {
                char[] inp = in.next().toCharArray();
                for(int j=1;j<m+1;j++){
                    mat[i][j]=inp[j-1];
                    if(mat[i][j]=='S') start.update(i, j);
                    if(mat[i][j]=='E') end.update(i, j);
                }
            }
            char[] ch = in.next().toCharArray();
//            for(int i=0;i<n+2;i++){
//                for(int j=0;j<m+2;j++){
//                    System.out.print(mat[i][j]);
//                }
//                System.out.println();
//            }
            int[] dx=new int[]{-1,0,1,0};
            int[] dy=new int[]{0,-1,0,1};
            int result=0;
            for (int a = 0; a < 4; a++) {
                for (int b = 0; b < 4; b++) {
                    for (int c = 0; c < 4; c++) {
                       for (int d = 0; d < 4; d++) {
                            if (!(a == b || a == c || a == d || b == c || b == d || c == d)) {
                                int[] comb= new int[]{a,b,c,d};
                                position curr= new position(start.x, start.y);
                                int i=0;
                                for(i=0;i<ch.length;i++){
                                    curr.update(dx[comb[ch[i]-'0']], dy[comb[ch[i]-'0']]);
                                    if(mat[curr.x][curr.y]=='*' ||mat[curr.x][curr.y]=='#' || mat[curr.x][curr.y]=='E')    break;
                                }
                                if(mat[curr.x][curr.y]=='E')    result++;
                            }
                        }
                    }
                }
            }
            
            out.println(result);
        }

        int LOG2(long item) {
            int count = 0;
            while (item > 1) {
                item >>= 1;
                count++;
            }
            return count;
        }

        long power(long x, long n) {
            if (n <= 0) {
                return 1;
            }
            long y = power(x, n / 2);

            if ((n & 1) == 1) {
                return (((y * y) % mod) * x) % mod;
            }

            return (y * y) % mod;
        }

        long gcd(long a, long b) {
            a = Math.abs(a);
            b = Math.abs(b);
            return BigInteger.valueOf(a).gcd(BigInteger.valueOf(b)).longValue();
        }
    }
    
    static class position{
        int x, y;
        public position(int x, int y){
            this.x=x;
            this.y=y;
        }
        
        void update(int x, int y){
            this.x+=x;
            this.y+=y;
        }
    }

    static class InputReader {

        private InputStream stream;
        private byte[] buf = new byte[8192];
        private int curChar, snumChars;
        private SpaceCharFilter filter;

        public InputReader(InputStream stream) {
            this.stream = stream;
        }

        public int snext() {
            if (snumChars == -1) {
                throw new InputMismatchException();
            }
            if (curChar >= snumChars) {
                curChar = 0;
                try {
                    snumChars = stream.read(buf);
                } catch (IOException e) {
                    throw new InputMismatchException();
                }
                if (snumChars <= 0) {
                    return -1;
                }
            }
            return buf[curChar++];
        }

        public int nextInt() {
            int c = snext();
            while (isSpaceChar(c)) {
                c = snext();
            }
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            int res = 0;
            do {
                if (c < '0' || c > '9') {
                    throw new InputMismatchException();
                }
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }

        public long nextLong() {
            int c = snext();
            while (isSpaceChar(c)) {
                c = snext();
            }
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            long res = 0;
            do {
                if (c < '0' || c > '9') {
                    throw new InputMismatchException();
                }
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }

        public int[] nextIntArray(int n) {
            int a[] = new int[n];
            for (int i = 0; i < n; i++) {
                a[i] = nextInt();
            }
            return a;
        }

        public String readString() {
            int c = snext();
            while (isSpaceChar(c)) {
                c = snext();
            }
            StringBuilder res = new StringBuilder();
            do {
                res.appendCodePoint(c);
                c = snext();
            } while (!isSpaceChar(c));
            return res.toString();
        }

        public String next() {
            return readString();
        }

        public boolean isSpaceChar(int c) {
            if (filter != null) {
                return filter.isSpaceChar(c);
            }
            return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
        }

        public interface SpaceCharFilter {

            public boolean isSpaceChar(int ch);
        }
    }

}
