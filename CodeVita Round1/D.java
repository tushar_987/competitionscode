//package tcscodevita;


import java.io.*;
import java.util.*;
import java.math.*;

/**
 *
 * @author Tushar 29 Jul, 2017
 */
class D {
    public static void main(String args[])
    {

        //File file= new File("COMPLETE_FILE_PATH");
        //InputStream is = new FileInputStream(file);
        
        
        //InputReader in = new InputReader(is);
        InputReader in = new InputReader(System.in);
        PrintWriter out= new PrintWriter(System.out);
        
        int t=1;
        Task solver=new Task();
//            long start = System.nanoTime();
        solver.solve(t,in,out);
//            long stop = System.nanoTime();
//            System.out.println("Execution time: " + ((stop - start) / 1e+6) + "ms.");
        

        out.flush();
        out.close();


    }

    static class Task {
        long mod = 1000000007;
        int x;
        int y;
        static boolean[] isPrime;
        int[] prime;

        public void solve(int testNumber, InputReader in,PrintWriter out){
            int n=in.nextInt();
            prime=new int[100000];
            sieveOptimized(1000000);
            
            
            
            List<Integer> numofSquares=new ArrayList<>();
            numofSquares.add(1);
            int sum=1;
            int num=0;
            while(sum<1000000){
                num+=8;
                sum+=num;
                numofSquares.add(sum);
            }
            
            for(int t=0;t<n;t++){
            int input=in.nextInt();
            
            
            int PrimePos=Arrays.binarySearch(prime,input);
            PrimePos++;
//            out.println(PrimePos);
            
            if(PrimePos==1){
                out.println(0+" "+0);
                continue;
            }
            x=0;y=0;
            
            
            
            
//            for(int i:numofSquares){
//                out.println(i);
//            }
            
            int number=BinarySearchUpperBound(numofSquares, PrimePos);
            if(numofSquares.contains(PrimePos)){
                number--;
            }
            int length_side=(number+1)*2;
            int curr_sq=PrimePos-numofSquares.get(number);
            
//            out.println("Curr_sq"+curr_sq);
//            out.println("pos"+curr_sq%length_side);
            
            findIndex((curr_sq-1)/length_side,(curr_sq-1)%length_side,number,length_side);

            out.println(x+" "+y);
            }
        }
        
        void findIndex(int side,int pos,int number,int length_side){
            if(side==0){
                x=number+1;
                y=pos-number;
            }
            else if(side==1){
                x=(pos-number)*-1;
                y=number+1;
            }
            else if(side==2){
                x=(number)*(-1)-1;
                y=(pos-number)*-1;
            }
            else if(side==3){
                x=(pos-number);
                y=number*(-1)-1;
            }
            
        }
        
       void sieveOptimized(int N) {
            isPrime = new boolean[N + 1];

            for(int i=2; i <= N; i++){
                isPrime[i] = true;
            }
            for (int i = 2; i * i <= N; i++) {
                if (isPrime[i]) {
                // For further optimization, You can do instead of j += i, j += (2 * i).
                // Proof is left to reader :)
                for (int j = i * i; j <= N; j += i) 
                    isPrime[j] = false;
            }
            }
            int index=0;
            for (int i = 2; i <= N; i++) {
                if (isPrime[i]) {
                    prime[index]=i;
                    index++;}                
            }
            
        }
        
        int BinarySearchUpperBound(List<Integer> lst,int value){
            int low=0;int hi=lst.size();
            
            while(hi-low>1){
                int mid=(hi+low)/2;
                if(lst.get(mid)<=value)   low=mid;
                else hi=mid;
                
                
            }
            return low;
        }
            
        int LOG2(long item){
            int count=0;
            while(item>1){
                item>>=1;count++;
            }
            return count;
        }

        long power(long x, long n) {
            if (n <= 0) {
                return 1;
            }
            long y = power(x, n / 2);

            if ((n & 1) == 1) {
                return (((y * y) % mod) * x) % mod;
            }

            return (y * y) % mod;
        }

        long gcd(long a, long b) {
            a = Math.abs(a);
            b = Math.abs(b);
            return BigInteger.valueOf(a).gcd(BigInteger.valueOf(b)).longValue();
        }
    }
    
    
    static class InputReader {
 
        private InputStream stream;
        private byte[] buf = new byte[8192];
        private int curChar, snumChars;
        private SpaceCharFilter filter;
 
        public InputReader(InputStream stream) {
            this.stream = stream;
        }
 
        public int snext() {
            if (snumChars == -1)
                throw new InputMismatchException();
            if (curChar >= snumChars) {
                curChar = 0;
                try {
                    snumChars = stream.read(buf);
                } catch (IOException e) {
                    throw new InputMismatchException();
                }
                if (snumChars <= 0)
                    return -1;
            }
            return buf[curChar++];
        }
 
        public int nextInt() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            int res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public long nextLong() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            long res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public int[] nextIntArray(int n) {
            int a[] = new int[n];
            for (int i = 0; i < n; i++)
                a[i] = nextInt();
            return a;
        }
 
        public String readString() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            StringBuilder res = new StringBuilder();
            do {
                res.appendCodePoint(c);
                c = snext();
            } while (!isSpaceChar(c));
            return res.toString();
        }

        public String next() {
            return readString();
        }
 
        public boolean isSpaceChar(int c) {
            if (filter != null)
                return filter.isSpaceChar(c);
            return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
        }
 
        public interface SpaceCharFilter {
            public boolean isSpaceChar(int ch);
        }
    }


}
