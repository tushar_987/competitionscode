//package educational_round_27;
//  Thanks to -> http://codeforces.com/contest/845/submission/29647921
import java.io.*;
import java.util.*;
import java.math.*;

//package linux_coding;

/**
 *
 * @author ghost
 */
public class abc {

 
    public static void main(String args[])
    {
 
        //File file= new File("COMPLETE_FILE_PATH");
        //InputStream is = new FileInputStream(file);
        
        
        //InputReader in = new InputReader(is);
        InputReader in = new InputReader(System.in);
        PrintWriter out= new PrintWriter(System.out);
        
        int t=1;
        
        Task solver=new Task();
        solver.solve(t,in,out);
        
 
        out.flush();
        out.close();
 
 
    }
 
    static class Task {
        long mod = 1000000007;
 
        public void solve(int testNumber, InputReader in,PrintWriter out){
            char[] str= in.next().toCharArray();
            int[] a= new int[3];
            int[] b= new int[3];
            
            int sum_a=0;int sum_b =0;
            for(int i=0;i<3;i++){    a[i]=str[i]-'0';   sum_a+=a[i];}
            for(int i=3;i<6;i++){    b[i-3]=str[i]-'0';   sum_b+=b[i-3];}
            
            if(sum_a==sum_b)    out.println(0);
            else if(sum_a<sum_b) out.println(count(a,b,sum_a,sum_b));
            else    out.println(count(b,a,sum_b,sum_a));
            
            
            
        }
        
        int count(int[] arr,int[] arr2,int less,int greater){
            List<Integer> li = new ArrayList<>();
            
            for(int i=0;i<3;i++)    li.add(9-arr[i]);
            for(int i=0;i<3;i++)    li.add(arr2[i]);
            
            Collections.sort(li,new DescendingComparator() );
            int index=0;
            int count=0;
            int diff= greater-less;
            while(index<li.size() && diff>0){
                diff-=li.get(index++);
                count++;
            }
            return count;
        }
        
        class DescendingComparator implements Comparator<Object>{
            @Override
            public int compare(Object o1,Object o2){
                if((int)o1 > (int)o2)    return -1;
                else if((int)o1 < (int)o2)    return 1;
                else return 0;
            }
        }
        
       
        
        int LOG2(long item){
            int count=0;
            while(item>1){
                item>>=1;count++;
            }
            return count;
        }
 
 
        long gcd(long a, long b) {
            a = Math.abs(a);
            b = Math.abs(b);
            return BigInteger.valueOf(a).gcd(BigInteger.valueOf(b)).longValue();
        }
    }
    
    
    static class InputReader {
 
        private InputStream stream;
        private byte[] buf = new byte[8192];
        private int curChar, snumChars;
        private SpaceCharFilter filter;
 
        public InputReader(InputStream stream) {
            this.stream = stream;
        }
 
        public int snext() {
            if (snumChars == -1)
                throw new InputMismatchException();
            if (curChar >= snumChars) {
                curChar = 0;
                try {
                    snumChars = stream.read(buf);
                } catch (IOException e) {
                    throw new InputMismatchException();
                }
                if (snumChars <= 0)
                    return -1;
            }
            return buf[curChar++];
        }
 
        public int nextInt() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            int res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public long nextLong() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            long res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public int[] nextIntArray(int n) {
            int a[] = new int[n];
            for (int i = 0; i < n; i++)
                a[i] = nextInt();
            return a;
        }
 
        public String readString() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            StringBuilder res = new StringBuilder();
            do {
                res.appendCodePoint(c);
                c = snext();
            } while (!isSpaceChar(c));
            return res.toString();
        }
 
        public String next() {
            return readString();
        }
 
        public boolean isSpaceChar(int c) {
            if (filter != null)
                return filter.isSpaceChar(c);
            return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
        }
 
        public interface SpaceCharFilter {
            public boolean isSpaceChar(int ch);
        }

        private static void pa(Object... o) {
            System.out.println(Arrays.deepToString(o));
        }
    }

}

