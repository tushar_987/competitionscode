//Simple Program to learn HashMap
import java.util.*;

/**
 *
 * @author Tushar Sep 21, 2016
 */
class HashMap_Demo {
    
    public static void main(String args[])
    {
        Scanner sc= new Scanner(System.in);
        Map<String,Integer> hm= new HashMap();
        
        hm.put("Tushar",1);
        hm.put("Gupta", 2);
        hm.put("Devesh", 3);
        hm.put("Pandey", 4);
        hm.put("Sachin", 5);
        hm.put("Sachi", 6);
        
        
        Set<Map.Entry<String,Integer>> set=hm.entrySet();
        
        for(Map.Entry<String,Integer> a : set)
        {
            System.out.print(a.getKey()+" : ");
            System.out.println(a.getValue());
        }
        
        hm.put("Tushar", hm.get("Tushar")+100);
        
        System.out.println("after increment:\nTushar :"+hm.get("Tushar"));
        
        
        
        //User Input inside a map
            
        Map<Integer,Integer> map=new HashMap<Integer,Integer>();
        
        int size=sc.nextInt();
        int value,key;
        for(int i=0;i<size;i++){
            key=sc.nextInt();
            if(map.containsKey(key))
                value=map.get(key);
            else value=0;
            map.put(key,value+1);
            
        }
        
        
        
        System.out.println(map);
        
        
        
    }

}
