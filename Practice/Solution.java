import java.util.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] score = new int[n];
        for(int score_i=0; score_i < n; score_i++){
            score[score_i] = in.nextInt();
        }
        
        int min=score[0],max=score[0];
        int countMin=0,countMax=0;
        
        for(int i=1;i<n;i++){
            if(score[i]>max){
                ++countMax;
                max=score[i];
            }
            else if(score[i]<min){
                ++countMin;
                min=score[i];
            }
        }
        
        System.out.println(countMax+" "+countMin);
    }
}
