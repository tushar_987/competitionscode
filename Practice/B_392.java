//package practice_problems;

import java.io.*;
import java.util.*;
import java.math.*;

/**
 *
 * @author Tushar Jan 19, 2017
 */
public class B_392 {

    long mod = 1000000007;


    public static void main(String args[])
    {
        InputReader in = new InputReader(System.in);
        PrintWriter out= new PrintWriter(System.out);

        int t=1;
        while(t-->0)
        {
            String str= in.next();
            
            int length=str.length();
            
            char[] arr= new char[length];
            arr=str.toCharArray();
            
            
            int[] ans= new int[4];
            for(int i=0;i<arr.length;i++){
                while(i<arr.length && arr[i]=='!')
                    i++;
                if(i<arr.length){
                    if(arr[i]=='R')
                        ans[0]=i;
                    else if(arr[i]=='B')
                        ans[1]=i;
                    else if(arr[i]=='Y')
                        ans[2]=i;
                    else if(arr[i]=='G')
                        ans[3]=i;
                }
                    
            }
            
//            for(int g:ans){
//                out.print(g);
//            }
            
            int count;
            int i;
            for(int k=0;k<4;k++){
            count=0;
            i=ans[k];
            while(i>=0){
                if(arr[i]=='!'){
                    count++;
                }
                i=i-4;
            }
            i=ans[k];
            while(i<arr.length){
                if(arr[i]=='!'){
                    count++;
                }
                i=i+4;
            }
            ans[k]=count;
            }
            for(int f:ans){
                out.print(f+" ");
            }
                
            
            
            
            

        }

        out.flush();
        out.close();


    }




    long power(long x, long n) {
        if (n <= 0) {
            return 1;
        }
        long y = power(x, n / 2);
 
        if ((n & 1) == 1) {
            return (((y * y) % mod) * x) % mod;
        }
 
        return (y * y) % mod;
    }
 
    public long gcd(long a, long b) {
        a = Math.abs(a);
        b = Math.abs(b);
        return BigInteger.valueOf(a).gcd(BigInteger.valueOf(b)).longValue();
    }
    
    
    static class InputReader {
 
        private InputStream stream;
        private byte[] buf = new byte[8192];
        private int curChar, snumChars;
        private SpaceCharFilter filter;
 
        public InputReader(InputStream stream) {
            this.stream = stream;
        }
 
        public int snext() {
            if (snumChars == -1)
                throw new InputMismatchException();
            if (curChar >= snumChars) {
                curChar = 0;
                try {
                    snumChars = stream.read(buf);
                } catch (IOException e) {
                    throw new InputMismatchException();
                }
                if (snumChars <= 0)
                    return -1;
            }
            return buf[curChar++];
        }
 
        public int nextInt() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            int res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public long nextLong() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            long res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public int[] nextIntArray(int n) {
            int a[] = new int[n];
            for (int i = 0; i < n; i++)
                a[i] = nextInt();
            return a;
        }
 
        public String readString() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            StringBuilder res = new StringBuilder();
            do {
                res.appendCodePoint(c);
                c = snext();
            } while (!isSpaceChar(c));
            return res.toString();
        }

        public String next() {
            return readString();
        }
 
        public boolean isSpaceChar(int c) {
            if (filter != null)
                return filter.isSpaceChar(c);
            return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
        }
 
        public interface SpaceCharFilter {
            public boolean isSpaceChar(int ch);
        }
    }

}
