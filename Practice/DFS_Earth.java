import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.InputMismatchException;
 
//package acm_practice;
/**
 *
 * @author ghost
 */
class Abc {
 
    static class Task {
 
        long mod = 1000000007;
        String ref = "abcdefghijklmnopqrstuvwxyz";
 
        public void solve(int testNumber, InputReader in, PrintWriter out) {
            int n = in.nextInt();
            int a = in.nextInt();
 
            if (n == 1) {
                out.println(1 + " a");
                return;
            }
 
            if (a == 1) {
                out.print(n + " ");
                for (int i = 0; i < n; i++) {
                    out.print("a");
                }
                out.println();
            } else if (a >= 3) {
                out.print(1 + " ");
 
                String add = "";
                add = add.concat(ref.substring(0, a));
                StringBuilder str = new StringBuilder(add);
                for (int i = 1; i < (n / a); i++) {
                    str = str.append(add);
                }
 
                if (str.length() < n) {
                    str = str.append(add.substring(0, n - str.length()));
                }
                out.println(str.substring(0, n));
            } else {
                if(n==2)
                    out.println(1 + " ab");
                else if(n==3)
                    out.println(2 + " abb");
                else if(n==4)    
                    out.println(2 + " aabb");
                else if(n==5)
                    out.println(3 + " aaabb");
                else if(n==6)
                    out.println(3 + " aababb");
                else if(n==7)
                    out.println(3 + " aaababb");
                else if(n==8)
                    out.println(3 + " aaababbb");
                else if(n==9)
                    out.println(4 + " aaababbbb");
                else if(n==10)
                    out.println(4 + " aaaababbbb");
                else{
                    String str1="aaaa";
                    String str2="babbaa";
                    StringBuilder result=new StringBuilder("aaaa");
                    
                    while((result.length())<n)    result.append("babbaa");
                    
//                    if(result.length()<n)   result.append("aaaa");
                    out.println(4+" "+result.substring(0,n));
                    
                }
            }
 
        }
 
        String palindromString(int size) {
            StringBuilder str = new StringBuilder();
            String add = "ab";
 
            for (int i = 0; i < size; i += 2) {
                str = str.append(add);
            }
 
            return str.substring(0, size);
        }
 
        int LOG2(long item) {
            int count = 0;
            while (item > 1) {
                item >>= 1;
                count++;
            }
            return count;
        }
 
        long gcd(long a, long b) {
            a = Math.abs(a);
            b = Math.abs(b);
            return BigInteger.valueOf(a).gcd(BigInteger.valueOf(b)).longValue();
        }
    }
 
    static class InputReader {
 
        private InputStream stream;
        private byte[] buf = new byte[8192];
        private int curChar, snumChars;
        private SpaceCharFilter filter;
 
        public InputReader(InputStream stream) {
            this.stream = stream;
        }
 
        public int snext() {
            if (snumChars == -1) {
                throw new InputMismatchException();
            }
            if (curChar >= snumChars) {
                curChar = 0;
                try {
                    snumChars = stream.read(buf);
                } catch (IOException e) {
                    throw new InputMismatchException();
                }
                if (snumChars <= 0) {
                    return -1;
                }
            }
            return buf[curChar++];
        }
 
        public int nextInt() {
            int c = snext();
            while (isSpaceChar(c)) {
                c = snext();
            }
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            int res = 0;
            do {
                if (c < '0' || c > '9') {
                    throw new InputMismatchException();
                }
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public long nextLong() {
            int c = snext();
            while (isSpaceChar(c)) {
                c = snext();
            }
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            long res = 0;
            do {
                if (c < '0' || c > '9') {
                    throw new InputMismatchException();
                }
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public int[] nextIntArray(int n) {
            int a[] = new int[n];
            for (int i = 0; i < n; i++) {
                a[i] = nextInt();
            }
            return a;
        }
 
        public String readString() {
            int c = snext();
            while (isSpaceChar(c)) {
                c = snext();
            }
            StringBuilder res = new StringBuilder();
            do {
                res.appendCodePoint(c);
                c = snext();
            } while (!isSpaceChar(c));
            return res.toString();
        }
 
        public String next() {
            return readString();
        }
 
        public boolean isSpaceChar(int c) {
            if (filter != null) {
                return filter.isSpaceChar(c);
            }
            return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
        }
 
        public interface SpaceCharFilter {
 
            public boolean isSpaceChar(int ch);
        }
 
        private static void pa(Object... o) {
            System.out.println(Arrays.deepToString(o));
        }
    }
 
    public static void main(String args[]) {
 
        //File file= new File("COMPLETE_FILE_PATH");
        //InputStream is = new FileInputStream(file);
        //InputReader in = new InputReader(is);
        InputReader in = new InputReader(System.in);
        PrintWriter out = new PrintWriter(System.out);
 
        int t = in.nextInt();
        while (t-- > 0) {
            Task solver = new Task();
            solver.solve(t, in, out);
        }
        out.flush();
        out.close();
 
    }
 
}