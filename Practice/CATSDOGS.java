//package practice_problems;

import java.io.*;

/**
 *
 * @author
 */
class CATSDOGS {
    
    public static void main(String arg[]) throws IOException
    {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        PrintWriter out= new PrintWriter(System.out);
        
        int t=Integer.parseInt(br.readLine());
        while(t>0)
        {
            int C,D,L;
            String[] input= br.readLine().split(" ");
            C=Integer.parseInt(input[0]);
            D=Integer.parseInt(input[1]);
            L=Integer.parseInt(input[2]);
            
            if(L%4!=0){
                out.println("no");
            }
            else if((D*4+C*4)<L){
                out.println("no");
            }
            else{
                L=L/4;
                
                if(L==D && C<=2*D){
                    out.print("yes\n");
                }
                else if(L>D){
                    L=L-D;
                    int afford=D*2;
                    
                    int CL=0;
                    if(afford<C){
                    CL=C-afford;
                    }

                    if(CL==0){
                        out.print("yes\n");
                    }
                    else if(L>=CL)
                    {
                        out.print("yes\n");
                    }
                    else{
                        out.print("no\n");
                    }
                }
                else{
                    out.print("no\n");
                }
            }
            
            
            t--;
            out.flush();
        }
        
        
        
        out.close();
        
        
    }
}