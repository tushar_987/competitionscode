//package practice_problems;

import java.io.*;
import java.util.*;
import java.math.*;

/**
 *
 * @author Tushar Feb 2, 2017
 */
public class B_394 {

    long mod = 1000000007;


    public static void main(String args[])
    {

        //File file= new File("COMPLETE_FILE_PATH");
        //InputStream is = new FileInputStream(file);
        
        
        //InputReader in = new InputReader(is);
        InputReader in = new InputReader(System.in);
        PrintWriter out= new PrintWriter(System.out);

        int t=1;
        while(t-->0)
        {
            int n=in.nextInt();
            int l=in.nextInt();
            
            int[] lefa= new int[n];
            int[] shasha= new int[n];
            int temp;
            //LEFA ARRAY
            for(int i=0;i<n;i++)
                lefa[i]=in.nextInt();
            
            temp=l-lefa[n-1]+lefa[0];
            for(int i=0;i<n-1;i++){
                lefa[i]=lefa[i+1]-lefa[i];
            }
            lefa[n-1]=temp;
//            Arrays.sort(lefa);
            
            
            //SHASHA ARRAY
            for(int i=0;i<n;i++)
                shasha[i]=in.nextInt();
            
            temp=l-shasha[n-1]+shasha[0];
            for(int i=0;i<n-1;i++){
                shasha[i]=shasha[i+1]-shasha[i];
            }
            shasha[n-1]=temp;
//            Arrays.sort(shasha);
            
            //Final Answer
            boolean ans=true;
            
//            for(int i=0;i<n;i++)
//                out.println(lefa[i]+"-->"+shasha[i]);
//            for(int i=0;i<n;i++)
//            {
//                if(lefa[i]!=shasha[i])
//                    ans=false;
//            }
            int flag=lefa[0];
            int i;
            for(i=0;i<n;i++)//fetching First Index
            {
                if(shasha[i]==flag)
                    break;
            }
            int j=i;
            i=1;
            for(;i<n;j++){
                if(shasha[j%l]!=lefa[i++])
                    ans=false;
                
            }
            
            out.print(ans?"YES":"NO");
        }

        out.flush();
        out.close();


    }



    long power(long x, long n) {
        if (n <= 0) {
            return 1;
        }
        long y = power(x, n / 2);
 
        if ((n & 1) == 1) {
            return (((y * y) % mod) * x) % mod;
        }
 
        return (y * y) % mod;
    }
 
    public long gcd(long a, long b) {
        a = Math.abs(a);
        b = Math.abs(b);
        return BigInteger.valueOf(a).gcd(BigInteger.valueOf(b)).longValue();
    }
    
    
    static class InputReader {
 
        private InputStream stream;
        private byte[] buf = new byte[8192];
        private int curChar, snumChars;
        private SpaceCharFilter filter;
 
        public InputReader(InputStream stream) {
            this.stream = stream;
        }
 
        public int snext() {
            if (snumChars == -1)
                throw new InputMismatchException();
            if (curChar >= snumChars) {
                curChar = 0;
                try {
                    snumChars = stream.read(buf);
                } catch (IOException e) {
                    throw new InputMismatchException();
                }
                if (snumChars <= 0)
                    return -1;
            }
            return buf[curChar++];
        }
 
        public int nextInt() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            int res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public long nextLong() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            long res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public int[] nextIntArray(int n) {
            int a[] = new int[n];
            for (int i = 0; i < n; i++)
                a[i] = nextInt();
            return a;
        }
 
        public String readString() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            StringBuilder res = new StringBuilder();
            do {
                res.appendCodePoint(c);
                c = snext();
            } while (!isSpaceChar(c));
            return res.toString();
        }

        public String next() {
            return readString();
        }
 
        public boolean isSpaceChar(int c) {
            if (filter != null)
                return filter.isSpaceChar(c);
            return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
        }
 
        public interface SpaceCharFilter {
            public boolean isSpaceChar(int ch);
        }
    }

}
