import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.StringTokenizer;

public class B_762_Soln {

  static class Mouse {
    long val;
    int type;
    public Mouse (long val, int type) {
      this.val = val;
      this.type = type;
    }
  }
  
  public static class AscendingComp implements Comparator<Mouse>{

        @Override
        public int compare(Mouse o1,Mouse o2) {
            if(o1.val<o2.val) return -1;
            else if(o1.val > o2.val) return 1;
            else return 0;
//            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

      
  }

  public static final int USB = 0, PS2 = 1;

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String s = br.readLine();
        StringTokenizer st = new StringTokenizer(s);
        int a = Integer.parseInt(st.nextToken());
        int b = Integer.parseInt(st.nextToken());
        int c = Integer.parseInt(st.nextToken());

        st = new StringTokenizer(br.readLine());
        int m = Integer.parseInt(st.nextToken());

        List<Mouse> list = new ArrayList<>();

        for (int i = 0; i < m; ++i) {
          st = new StringTokenizer(br.readLine());
          Mouse mouse = new Mouse(Integer.parseInt(st.nextToken()), st.nextToken().equals("USB") ? USB : PS2);
          list.add(mouse);
        }

//        Collections.sort(list, (m1, m2) ->  m1.val < m2.val ? -1 : 1);
        Collections.sort(list,new AscendingComp());

        int count = 0;
        long minVal = 0L;

        for (int i = 0; i < m; ++i) {
          long val = list.get(i).val;
          int type = list.get(i).type;
          if (type == USB) {
            if (a > 0) {
              --a;
              ++count;
              minVal+=val;
            } else if (c > 0) {
              --c;
              ++count;
              minVal+=val;
            }
          } else {
            if (b > 0) {
              --b;
              ++count;
              minVal+=val;
            } else if (c > 0) {
              --c;
              ++count;
              minVal+=val;
            }
          }
        }

        System.out.println(count + " " + minVal);



    }
}