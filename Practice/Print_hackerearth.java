
import java.util.*;



/**
 *
 * @author Tushar Sep 17, 2016
 */
class Print_hackerearth {
    
    static int min(int[] arr)
    {
        int minimum=arr[0];
        for(int i=1;i<arr.length;i++)
        {
            if(arr[i]<minimum)
                minimum=arr[i];
        }
        
        return minimum;
    }
    
    public static void main(String arg[])
    {
        Scanner sc= new Scanner(System.in);
        
        int size=sc.nextInt();
        char[] ch=new char[size];
        ch=sc.next().toCharArray();
        
        int[] var={0,0,0,0,0,0,0};
        for(int i=0;i<ch.length;i++)
        {
            if(ch[i]=='a')
                var[0]++;
            else if(ch[i]=='e')
                var[1]++;
            else if(ch[i]=='h')
                var[2]++;
            else if(ch[i]=='c')
                var[3]++;
            else if(ch[i]=='k')
                var[4]++;
            else if(ch[i]=='r')
                var[5]++;
            else if(ch[i]=='t')
                var[6]++; 
            
        }
        
        var[0]/=2;
        var[1]/=2;
        var[2]/=2;
        var[5]/=2;
        
        System.out.println(min(var));
        
    }

}
