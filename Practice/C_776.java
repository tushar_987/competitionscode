//package practice_problems;

import java.io.*;
import java.util.*;
import java.math.*;

/**
 *
 * @author Tushar Feb 23, 2017
 */
public class C_776 {

    public static void main(String args[])
    {

        //File file= new File("COMPLETE_FILE_PATH");
        //InputStream is = new FileInputStream(file);
        
        
        //InputReader in = new InputReader(is);
        InputReader in = new InputReader(System.in);
        PrintWriter out= new PrintWriter(System.out);
        
        int t=1;
        
        Task solver=new Task();
        solver.solve(t,in,out);
        

        out.flush();
        out.close();


    }

    static class Task {
        long mod = 1000000007;

        public void solve(int testNumber, InputReader in,PrintWriter out){
            int n=in.nextInt();
            int k=Math.abs(in.nextInt());
            
            Set<Long> set= new HashSet<>();
            
            for(int i=1;i<=40;i++)
                set.add(power(k,i));
            
            int[] arr=new int[n+1];
            for(int i=1;i<=n;i++)
                arr[i]=in.nextInt();
            
            long[][] mat=new long[n+1][n+1];
            
            long count=0;
            for(int f=1;f<=n;f++)
                for(int i=f,j=1;i<=n;i++,j++){
                    if(i==j){
                        mat[i][j]=arr[i];
                        }
                    else
                        mat[i][j]=mat[i-1][j]+arr[i];
                    
                    if(mat[i][j]==1)
                        ++count;
                    else if(set.contains(mat[i][j]))
                            ++count;
            }
            
            
//            for(int i=1;i<=n;i++){
//                for(int j=1;j<=n;j++)
//                    out.print(mat[i][j]);
//            out.println();
//            }      
            
            out.println(count);
            

        }
            
        int LOG2(long item){
            int count=0;
            while(item>1){
                item>>=1;count++;
            }
            return count;
        }

        long power(long x, long n) {
            if (n <= 0) {
                return 1;
            }
            long y = power(x, n / 2);

            if ((n & 1) == 1) {
                return (((y * y) % mod) * x) % mod;
            }

            return (y * y) % mod;
        }

        long gcd(long a, long b) {
            a = Math.abs(a);
            b = Math.abs(b);
            return BigInteger.valueOf(a).gcd(BigInteger.valueOf(b)).longValue();
        }
    }
    
    
    static class InputReader {
 
        private InputStream stream;
        private byte[] buf = new byte[8192];
        private int curChar, snumChars;
        private SpaceCharFilter filter;
 
        public InputReader(InputStream stream) {
            this.stream = stream;
        }
 
        public int snext() {
            if (snumChars == -1)
                throw new InputMismatchException();
            if (curChar >= snumChars) {
                curChar = 0;
                try {
                    snumChars = stream.read(buf);
                } catch (IOException e) {
                    throw new InputMismatchException();
                }
                if (snumChars <= 0)
                    return -1;
            }
            return buf[curChar++];
        }
 
        public int nextInt() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            int res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public long nextLong() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            long res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public int[] nextIntArray(int n) {
            int a[] = new int[n];
            for (int i = 0; i < n; i++)
                a[i] = nextInt();
            return a;
        }
 
        public String readString() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            StringBuilder res = new StringBuilder();
            do {
                res.appendCodePoint(c);
                c = snext();
            } while (!isSpaceChar(c));
            return res.toString();
        }

        public String next() {
            return readString();
        }
 
        public boolean isSpaceChar(int c) {
            if (filter != null)
                return filter.isSpaceChar(c);
            return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
        }
 
        public interface SpaceCharFilter {
            public boolean isSpaceChar(int ch);
        }
    }

}
