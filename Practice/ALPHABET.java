import java.io.*;
import java.util.Scanner;


/**
 *
 * @author Tushar Aug 28, 2016
 */
class ALPHABET {
    
    public static void main(String [] args) throws IOException
    {
        Scanner sc= new Scanner(System.in);
        
        char[] s=sc.next().toCharArray();
        
        boolean[] alpha=new boolean[26];
        
        for(char c:s)
        {
            alpha[c-'a']=true;
        }
        int n=sc.nextInt();
        for(int i=0;i<n;i++)
        {
            s=sc.next().toCharArray();
            boolean ans=true;
            for(char c:s)
            {
                if(!alpha[c-'a'])
                {
                    ans=false;
                    break;
                }
            }
            System.out.println(ans?"Yes":"No");
            
        }
        
        
        
        
        
        
        
    }

}
