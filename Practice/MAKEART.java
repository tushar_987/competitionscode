/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package practice_problems;

import java.util.*;

/**
 *
 * @author This Pc
 */
class MAKEART {
    
    public static void main(String args[])
    {
        Scanner sc=new Scanner(System.in);
        int t;
        t=sc.nextInt();
        while(t-->0)
        {
            int n=sc.nextInt();
            int arr[]= new int[n];
            int i;
            for(i=0;i<n;i++){
                arr[i]=sc.nextInt();
                
            }
            int flag=0;
            for(i=0;i<n-2;i++){
                if((arr[i]==arr[i+1])&&(arr[i+1]==arr[i+2])){
                    flag++;
                    break;
                }
            }
            
            System.out.println(flag==0?"No":"Yes");
            
            
        
        }
    }
    
}
