//package practice_problems;

import java.io.*;
import java.math.*;
import java.util.*;

/**
 *
 * @author Tushar Feb 10, 2017
 */
class Graph {
    
    

    long mod = 1000000007;
    
    private int V;
    private LinkedList<Integer>[] adj;
    private static int count_subtree;
    private static int[] number;
    
    
    Graph(int v){   // Constructor
        V=v;
        adj= new LinkedList[V+1];
        for(int i=1;i<=V;i++){
            adj[i]=new LinkedList<Integer>();
        }
        count_subtree=0;
        number=new int[V];
        Arrays.fill(number,0);
    }
    
    void addEdge(int v, int u){
        adj[v].add(u);
        adj[u].add(v);//becuase of Undirected graph
    }
    
    
    void DFS(){
        boolean[] visited= new boolean[V+1];
        Arrays.fill(visited, false);// WE dont need to beacause default is False
        
//        visited[v]=true;
//        count_subtree=1;
//        DFSUtil(v,visited);
        
        
        
        //Checking all the index
        for (int i=1;i<=V;i++){
            if(visited[i]==false){
                ++count_subtree;
                DFSUtil(i,visited);
                
            }
        }
    }
    
    
    void DFSUtil(int v,boolean visited[]){
        visited[v]=true;
        
        
        Iterator<Integer> i= adj[v].listIterator();
        
        while(i.hasNext()){
            int n=i.next();
            if(visited[n]==false){
                DFSUtil(n, visited);
            }
        }
        
        ++number[count_subtree];
//        System.out.println(v+" "+number[count_subtree]);
    }
    
    


    public static void main(String args[]) throws Exception
    {

        //File file= new File("COMPLETE_FILE_PATH");
        //InputStream is = new FileInputStream(file);
        
        
        //InputReader in = new InputReader(is);
//        InputReader in = new InputReader(System.in);
//        Scanner in= new Scanner(System.in);
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        PrintWriter out= new PrintWriter(System.out);
        

        int t=Integer.parseInt(in.readLine());
        while(t-->0)
        {
            
            String[] input= new String[2];
            input=in.readLine().split(" ");
            int n=Integer.parseInt(input[0]);
            Graph graph=new Graph(n);
            
            int m=Integer.parseInt(input[1]);
            
            for(int i=0;i<m;i++){
                input=in.readLine().split(" ");
                graph.addEdge(Integer.parseInt(input[0]),Integer.parseInt(input[1]));
            }
            
            graph.DFS();
            long ans=1;
            
            out.print(count_subtree+" ");
            for(int i=1;i<=count_subtree;i++){
                
                ans=ans*number[i];
//                out.print(number[i]+" " );
            }
            
           out.println(ans);

        }

        out.flush();
        out.close();


    }



    long power(long x, long n) {
        if (n <= 0) {
            return 1;
        }
        long y = power(x, n / 2);
 
        if ((n & 1) == 1) {
            return (((y * y) % mod) * x) % mod;
        }
 
        return (y * y) % mod;
    }
 
    public long gcd(long a, long b) {
        a = Math.abs(a);
        b = Math.abs(b);
        return BigInteger.valueOf(a).gcd(BigInteger.valueOf(b)).longValue();
    }
    
    
    static class InputReader {
 
        private InputStream stream;
        private byte[] buf = new byte[8192];
        private int curChar, snumChars;
        private SpaceCharFilter filter;
 
        public InputReader(InputStream stream) {
            this.stream = stream;
        }
 
        public int snext() {
            if (snumChars == -1)
                throw new InputMismatchException();
            if (curChar >= snumChars) {
                curChar = 0;
                try {
                    snumChars = stream.read(buf);
                } catch (IOException e) {
                    throw new InputMismatchException();
                }
                if (snumChars <= 0)
                    return -1;
            }
            return buf[curChar++];
        }
 
        public int nextInt() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            int res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public long nextLong() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            long res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public int[] nextIntArray(int n) {
            int a[] = new int[n];
            for (int i = 0; i < n; i++)
                a[i] = nextInt();
            return a;
        }
 
        public String readString() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            StringBuilder res = new StringBuilder();
            do {
                res.appendCodePoint(c);
                c = snext();
            } while (!isSpaceChar(c));
            return res.toString();
        }

        public String next() {
            return readString();
        }
 
        public boolean isSpaceChar(int c) {
            if (filter != null)
                return filter.isSpaceChar(c);
            return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
        }
 
        public interface SpaceCharFilter {
            public boolean isSpaceChar(int ch);
        }
    }

}
