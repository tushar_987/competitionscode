import java.io.PrintWriter;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;


/**
 *
 * @author Tushar Sep 17, 2016
 */
class SEPT_CIRCUIT {
    
    static int[] bits ;
    
    static class FastReader
	{
		final private int BUFFER_SIZE=1<<16;
		private DataInputStream din;
		private byte[] buffer;
		private int bufferPointer,bytesRead;
		
		public FastReader()
		{
			din=new DataInputStream(System.in);
			buffer=new byte[BUFFER_SIZE];
			bufferPointer=bytesRead=0;
		}
		
		public FastReader(String file_name) throws IOException
		{
			din=new DataInputStream(new FileInputStream(file_name));
		    buffer=new byte[BUFFER_SIZE];
		    bufferPointer=bytesRead=0;
		}
		
		private void fillBuffer() throws IOException
		{
			bytesRead =din.read(buffer,bufferPointer=0,BUFFER_SIZE);
		    if(bytesRead==-1)
		    	buffer[0]=-1;
		    
		}
		
		
		
		private byte read() throws IOException
		{
			if (bufferPointer==bytesRead)
				fillBuffer();
			return buffer[bufferPointer++];
		}
		
		public String next() throws IOException
		{
			byte[] buf=new byte[64];
			int cnt=0,c;
			while((c=read())!=-1)
			{
				if(c<=' ')
					break;
				buf[cnt++]=(byte)c; 
			}
			return new String(buf,0,cnt);
		}
		public String nextLine() throws IOException
		{
			byte[] buf=new byte[64];//line length
			int cnt=0,c;
			while((c=read())!=-1)
			{
				if(c=='\n')
					break;
				buf[cnt++]=(byte)c;
			}
			return new String(buf,0,cnt);
		}
		
		public int nextInt() throws IOException
		{
			int ret =0;
			byte c= read();
			while(c<=' ')
				c=read();
			boolean neg=(c=='-');
			if(neg)
				c=read();
			do
			{
				ret=ret*10+c-'0';
			}while((c=read())>='0'&& c<='9');
			if(neg)
				return -ret;
			return ret;
			
		}
		
		public long nextLong() throws IOException
		{
			long ret=0;
			byte c=read();
			while(c<=' ')
				c=read();
			boolean neg=(c=='-');
			if(neg)
				c=read();
			do
			{
				ret=ret*10+c-'0';
			}while((c=read())>='0'&& c<='9');
			if(neg)
				return -ret;
			return ret;
			
		}	
		
		public double nextDouble() throws IOException
		{
			double ret=0,div=1;
			byte c=read();
			while(c<=' ')
				c=read();
			boolean  neg=(c=='-');
			if(neg)
				c=read();
			do{
				ret=ret*10+c-'0';
			}
			while((c=read())>='0' && c<='9');
			
			if(c=='.')
			{
				while((c=read())>='0' && c<='9')
				{
					ret +=(c-'0')/(div*= 10);
				}
			}
			if(neg)
	             return -ret;
			return ret;
		}	
	
	}
    
    static int bitCount(int n)
    {
        int count=0;
        while(n!=0)
        {
            n=n>>1;
            count++;
        }
        
        return count;
    }
    
    
    public static void main(String arg[]) throws IOException
    {
        FastReader sc= new FastReader();
        PrintWriter out=new PrintWriter(System.out);
        
        
              
        int n=sc.nextInt();
        int q=sc.nextInt();
        
        int[] arr=new int[n];
       
        for(int i=0;i<arr.length;i++)
        {
            arr[i]=sc.nextInt();
        }
        
        bits=new int[n];
        bits[0]=bitCount(arr[0]);
        for(int i=1;i<n;i++)
        {
            bits[i]=bitCount(arr[i])+bits[i-1];
        }
        for(int i=0;i<q;i++)
        {
            
            int l=sc.nextInt();
            int r=sc.nextInt();
            
            int sum;
            if(l==1)
                sum=bits[r-1];
            else
                sum=bits[r-1]-bits[l-2];
            
          //  System.out.println(sum);
            
            if(sum%2!=0)
                out.println("Mishki");
            else
                out.println("Hacker");
                
                
           
            
        }
        
         out.flush();
            out.close();
    }

}
