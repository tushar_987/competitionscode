import java.io.*;
import java.util.Scanner;


class test
{
   static Scanner sc=new Scanner(System.in);
   static PrintWriter out=new PrintWriter(System.out);
	static boolean prime(int a)
	{
            if(a==1)
                return false;
            
            for(int i=2;i<=Math.sqrt(a);i++)
            {
                if(a%i==0)
                {
                    return false;
                }
            }
            
            return true;
		
	}
	
	public static void main(String[] args)
	{
		
		int t=sc.nextInt();
		while(t>0)
		{
			int n=sc.nextInt();
			int[] arr= new int[n];
			int high=-1;
			for(int i=0;i<n;i++)
			{
				arr[i]=sc.nextInt();
				if(prime(arr[i]))
				{
					if(arr[i]>high)
					{
						high=arr[i];
					}
				}
			}
                        
                        if(high==-1)
                        {
                            out.println(-1);
                        }
                        else
                            out.println(high*high);
			
			
			
			
			t--;
		}
                
                out.close();
	}
}