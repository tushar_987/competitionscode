   /*C code on the same logic got submitted but there is some problem with this code. But the show must go on....*/
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
    /**
     *
     * @author This Pc
     */
    class CHEFARK {
        
        
        
        
        static class FastReader
	{
		final private int BUFFER_SIZE=1<<16;
		private DataInputStream din;
		private byte[] buffer;
		private int bufferPointer,bytesRead;
		
		public FastReader()
		{
			din=new DataInputStream(System.in);
			buffer=new byte[BUFFER_SIZE];
			bufferPointer=bytesRead=0;
		}
		
		public FastReader(String file_name) throws IOException
		{
			din=new DataInputStream(new FileInputStream(file_name));
		    buffer=new byte[BUFFER_SIZE];
		    bufferPointer=bytesRead=0;
		}
		
		private void fillBuffer() throws IOException
		{
			bytesRead =din.read(buffer,bufferPointer=0,BUFFER_SIZE);
		    if(bytesRead==-1)
		    	buffer[0]=-1;
		    
		}
		
		
		
		private byte read() throws IOException
		{
			if (bufferPointer==bytesRead)
				fillBuffer();
			return buffer[bufferPointer++];
		}
		
		public String next() throws IOException
		{
			byte[] buf=new byte[64];
			int cnt=0,c;
			while((c=read())!=-1)
			{
				if(c<=' ')
					break;
				buf[cnt++]=(byte)c; 
			}
			return new String(buf,0,cnt);
		}
		public String nextLine() throws IOException
		{
			byte[] buf=new byte[64];//line length
			int cnt=0,c;
			while((c=read())!=-1)
			{
				if(c=='\n')
					break;
				buf[cnt++]=(byte)c;
			}
			return new String(buf,0,cnt);
		}
		
		public int nextInt() throws IOException
		{
			int ret =0;
			byte c= read();
			while(c<=' ')
				c=read();
			boolean neg=(c=='-');
			if(neg)
				c=read();
			do
			{
				ret=ret*10+c-'0';
			}while((c=read())>='0'&& c<='9');
			if(neg)
				return -ret;
			return ret;
			
		}
		
		public long nextLong() throws IOException
		{
			long ret=0;
			byte c=read();
			while(c<=' ')
				c=read();
			boolean neg=(c=='-');
			if(neg)
				c=read();
			do
			{
				ret=ret*10+c-'0';
			}while((c=read())>='0'&& c<='9');
			if(neg)
				return -ret;
			return ret;
			
		}	
		
		public double nextDouble() throws IOException
		{
			double ret=0,div=1;
			byte c=read();
			while(c<=' ')
				c=read();
			boolean  neg=(c=='-');
			if(neg)
				c=read();
			do{
				ret=ret*10+c-'0';
			}
			while((c=read())>='0' && c<='9');
			
			if(c=='.')
			{
				while((c=read())>='0' && c<='9')
				{
					ret +=(c-'0')/(div*= 10);
				}
			}
			if(neg)
	             return -ret;
			return ret;
		}	
	
	}
        static long[] factorial=new long[100005];
        final static int mod=1000000007;


        private static void fact()
        {
            factorial[0]=1;
            for(int i=1;i<=100002;i++)
                factorial[i]=(factorial[i-1]*i)%mod;
        }

        private static long inverse(long a)
    {
            long pow=mod-2;
            long act=a,res=1;
            while(pow>0)
            {
                    if(pow%2!=0)
                    {	
                            res*=act;
                            res%=mod;
                    }
                    act*=act;
                    act%=mod;
                    pow/=2;
            }
            return res;
    }

        public static void main(String[] args) throws IOException
        {
            long temp;
            boolean nz;
            
            FastReader scan=new FastReader();


            
            int t=scan.nextInt();
            
            fact();
            for(int l=0;l<t;l++)
            {
                long res=0;
                int n=scan.nextInt();
                int k=scan.nextInt();
                nz=false;
                
                String arr[]=scan.nextLine().split(" ");
                for(String s:arr)
                {
                    if(s.equals("0"))
                    {
                        nz=true;
                        n--;
                    }
                }
                
                for(int j=0;j<=Math.min(n, k);j++)
                {
                    if(nz || (k+j)%2==0)
                    {
                        temp=factorial[n];
                        temp*=inverse(factorial[n-j]);
                        temp%=mod;
                        temp*=inverse(factorial[j]);
                        temp%=mod;
                        res+=temp;
                    }
                }
                res%=mod;
                System.out.println(res);


            }
        }

    }
