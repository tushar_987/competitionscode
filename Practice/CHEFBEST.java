//package practice_problems;

import java.io.*;
import java.util.*;
import java.math.*;

/**
 *
 * @author Tushar Feb 4, 2017
 */
class CHEFBEST {

    long mod = 1000000007;


    public static void main(String args[]) throws FileNotFoundException
    {

//        File file= new File("C:\\Documents\\NetBeansProjects\\Practice_Problems\\src\\practice_problems\\CHEFBEST_test.txt");
//        InputStream is = new FileInputStream(file);
//        
//        
//        InputReader in = new InputReader(is);
        InputReader in = new InputReader(System.in);
        PrintWriter out= new PrintWriter(System.out);

        int t=in.nextInt();
        while(t-->0)
        {
            int n=in.nextInt();
            
            int first_0=0;
            boolean con=false;
            int last_1=0;
            int count=0;
            int ans=0;
            int[] input= new int[n+1];
                        
            for(int i=1;i<=n;i++){
                input[i]=in.nextInt();
                
                if(input[i]==0 && con==false){
                    first_0=i;
                    con=true;
                }
                else if(input[i]==1){
                    last_1=i;
                }
            }
            for(int i=first_0;i<last_1 && i+1<=n;i++){// number of Zero One Pairs
                if(input[i]==0 && input[i+1]==1){
                    count++;
                    i++;
                }
                else if(input[i]==1 && input[i+1]==0){
                    count++;
                    i++;
                }
            }
//            out.println("------"+count);
//            out.println(first_0);
//            out.println(count_0);
//            out.println(last_1);
            if(last_1==0 || first_0==0){// no zeroes and no one case
                out.println(0);
            }
            else{
                ans=ans+((last_1-first_0+1)-count);
                out.println(ans);
                
            }
            out.flush();
        }

        
        out.close();


    }



    long power(long x, long n) {
        if (n <= 0) {
            return 1;
        }
        long y = power(x, n / 2);
 
        if ((n & 1) == 1) {
            return (((y * y) % mod) * x) % mod;
        }
 
        return (y * y) % mod;
    }
 
    public long gcd(long a, long b) {
        a = Math.abs(a);
        b = Math.abs(b);
        return BigInteger.valueOf(a).gcd(BigInteger.valueOf(b)).longValue();
    }
    
    
    static class InputReader {
 
        private InputStream stream;
        private byte[] buf = new byte[8192];
        private int curChar, snumChars;
        private SpaceCharFilter filter;
 
        public InputReader(InputStream stream) {
            this.stream = stream;
        }
 
        public int snext() {
            if (snumChars == -1)
                throw new InputMismatchException();
            if (curChar >= snumChars) {
                curChar = 0;
                try {
                    snumChars = stream.read(buf);
                } catch (IOException e) {
                    throw new InputMismatchException();
                }
                if (snumChars <= 0)
                    return -1;
            }
            return buf[curChar++];
        }
 
        public int nextInt() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            int res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public long nextLong() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            long res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public int[] nextIntArray(int n) {
            int a[] = new int[n];
            for (int i = 0; i < n; i++)
                a[i] = nextInt();
            return a;
        }
 
        public String readString() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            StringBuilder res = new StringBuilder();
            do {
                res.appendCodePoint(c);
                c = snext();
            } while (!isSpaceChar(c));
            return res.toString();
        }

        public String next() {
            return readString();
        }
 
        public boolean isSpaceChar(int c) {
            if (filter != null)
                return filter.isSpaceChar(c);
            return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
        }
 
        public interface SpaceCharFilter {
            public boolean isSpaceChar(int ch);
        }
    }

}
