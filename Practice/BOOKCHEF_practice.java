//package practice_problems;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.InputMismatchException;

/**
 *
 * @author Tushar Oct 24, 2016
 */
class BOOKCHEF_practice {

    long mod = 1000000007;


    public static void main(String args[]) throws Exception
    {
        InputReader in = new InputReader(System.in);
        PrintWriter out= new PrintWriter(System.out);

        int t=1;
        while(t-->0)
        {        
        int n=in.nextInt();
        int m=in.nextInt();
        
        ArrayList<Integer> f= new ArrayList<>();
        ArrayList<Integer> spec= new ArrayList<Integer>();
        ArrayList<Integer> norm= new ArrayList<Integer>();
        
        
        HashMap<Integer,String> hm= new HashMap<>();
        
        
        for(int i=0;i<n;i++)
        {
            f.add(in.nextInt());
        }
        
        for(int i=0;i<m;i++)
        {            
            int index=in.nextInt();
            int pop=in.nextInt();
            
            if(f.contains(index))
                spec.add(pop);
            else
                norm.add(pop);
            
            hm.put(pop,in.next());
        }
            
            
            
        Collections.sort(spec, new DescendingComparator());//will sort ArrayList of Integers in descending order
        Collections.sort(norm, new DescendingComparator());//will sort ArrayList of Integers in descending order

            
        for(int j=0;j<spec.size();j++)
         {
               out.println(hm.get(spec.get(j)));
         }
        for(int j=0;j<norm.size();j++)
         {
               out.println(hm.get(norm.get(j)));
         }
        
     
        
        

        }
        
        out.flush();
        out.close();


    }
    
    
    public static class DescendingComparator implements Comparator{
   public int compare(Object o1, Object o2) /*descending order*/  {
      if (((Integer) o1).intValue() > ((Integer)o2).intValue()) return -1;
      else if (((Integer) o1).intValue() < ((Integer)o2).intValue()) return 1;
      return 0; /*equal*/
   }
}

    long power(long x, long n) {
        if (n <= 0) {
            return 1;
        }
        long y = power(x, n / 2);
 
        if ((n & 1) == 1) {
            return (((y * y) % mod) * x) % mod;
        }
 
        return (y * y) % mod;
    }
 
    public long gcd(long a, long b) {
        a = Math.abs(a);
        b = Math.abs(b);
        return BigInteger.valueOf(a).gcd(BigInteger.valueOf(b)).longValue();
    }
    
    
    static class InputReader {
 
        private InputStream stream;
        private byte[] buf = new byte[8192];
        private int curChar, snumChars;
        private SpaceCharFilter filter;
 
        public InputReader(InputStream stream) {
            this.stream = stream;
        }
 
        public int snext() {
            if (snumChars == -1)
                throw new InputMismatchException();
            if (curChar >= snumChars) {
                curChar = 0;
                try {
                    snumChars = stream.read(buf);
                } catch (IOException e) {
                    throw new InputMismatchException();
                }
                if (snumChars <= 0)
                    return -1;
            }
            return buf[curChar++];
        }
 
        public int nextInt() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            int res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public long nextLong() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            long res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public int[] nextIntArray(int n) {
            int a[] = new int[n];
            for (int i = 0; i < n; i++)
                a[i] = nextInt();
            return a;
        }
 
        public String readString() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            StringBuilder res = new StringBuilder();
            do {
                res.appendCodePoint(c);
                c = snext();
            } while (!isSpaceChar(c));
            return res.toString();
        }
 
        public boolean isSpaceChar(int c) {
            if (filter != null)
                return filter.isSpaceChar(c);
            return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
        }
 
        public interface SpaceCharFilter {
            public boolean isSpaceChar(int ch);
        }
        
        public String next() {
            return readString();
        }
    }

}
