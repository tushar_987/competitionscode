//package practice_problems;

import java.io.*;
import java.util.*;
import java.math.*;
import java.util.Map.Entry;

/**
 *
 * @author Tushar Oct 31, 2016
 */
public class B {

    long mod = 1000000007;


    public static void main(String args[])
    {
        InputReader in = new InputReader(System.in);
        PrintWriter out= new PrintWriter(System.out);

        
        Map<Integer,Integer> pos=new HashMap<>();
        Map<Integer,Integer> neg=new HashMap<>();
        
        int maxp=0,maxn=0;
        
        int t=in.nextInt();
        for(int i=1;i<=t;i++)
        {
            int l,r;
            
            l=in.nextInt();
            r=in.nextInt();
            if(l-r>0)
            {
                pos.put(i,l-r);
                if((l-r)>maxp)
                    maxp=l-r;
            }
            else
            {
                neg.put(i,r-l);
                if((r-l>maxn))
                    maxn=r-l;
            }
        }
        
        
        if(pos.size()>neg.size())
        {
            if(neg.size()>0)
            {
                for(Entry<Integer,Integer> e:neg.entrySet())
                {
                 if(e.getValue()==maxn){
                     out.print(e.getKey());
                     break;
                     
                 }
                }
                
                
            }
            else
                out.print(0);
        }
        else if(neg.size()>pos.size())
        {
            if(pos.size()>0)
            {
                for(Entry<Integer,Integer> e:pos.entrySet())
                {
                 if(e.getValue()==maxp){
                     out.print(e.getKey());
                     break;
                 }
                }
            }
            else
                out.print(0);
        }
        else
        {
            int sum1=0,sum2=0;
            for(Integer value:pos.keySet())
            {
                sum1=sum1+value;
            }
            for(Integer value:neg.keySet())
            {
                sum2=sum2+value;
            }
            if(sum1>sum2)
            {
               // out.print(neg.get(neg.lastKey())+"Eq");
            }
            else
               // out.print(pos.get(pos.lastKey())+"EQ");

        out.flush();
        out.close();


    }
    
    } 
    private static Map<Integer, Integer> sortByComparator(Map<Integer, Integer> unsortMap, final boolean order)
    {

        List<Entry<Integer, Integer>> list = new LinkedList<Entry<Integer, Integer>>(unsortMap.entrySet());

        // Sorting the list based on values
        Collections.sort(list, new Comparator<Entry<Integer, Integer>>()
        {
            public int compare(Entry<Integer, Integer> o1,
                    Entry<Integer, Integer> o2)
            {
                if (order)
                {
                    return o1.getValue().compareTo(o2.getValue());
                }
                else
                {
                    return o2.getValue().compareTo(o1.getValue());

                }
            }
        });

        // Maintaining insertion order with the help of LinkedList
        Map<Integer, Integer> sortedMap = new LinkedHashMap<Integer, Integer>();
        for (Entry<Integer, Integer> entry : list)
        {
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        return sortedMap;
    }
    
    
    public static void printMap(Map<Integer,Integer> map)
    {
        for(Entry<Integer,Integer> entry:map.entrySet())
        {
            System.out.println("Key"+entry.getKey()
                               +"Value :"+entry.getValue());
        }
    }



    long power(long x, long n) {
        if (n <= 0) {
            return 1;
        }
        long y = power(x, n / 2);
 
        if ((n & 1) == 1) {
            return (((y * y) % mod) * x) % mod;
        }
 
        return (y * y) % mod;
    }
 
    public long gcd(long a, long b) {
        a = Math.abs(a);
        b = Math.abs(b);
        return BigInteger.valueOf(a).gcd(BigInteger.valueOf(b)).longValue();
    }
    
    
    static class InputReader {
 
        private InputStream stream;
        private byte[] buf = new byte[8192];
        private int curChar, snumChars;
        private SpaceCharFilter filter;
 
        public InputReader(InputStream stream) {
            this.stream = stream;
        }
 
        public int snext() {
            if (snumChars == -1)
                throw new InputMismatchException();
            if (curChar >= snumChars) {
                curChar = 0;
                try {
                    snumChars = stream.read(buf);
                } catch (IOException e) {
                    throw new InputMismatchException();
                }
                if (snumChars <= 0)
                    return -1;
            }
            return buf[curChar++];
        }
 
        public int nextInt() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            int res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public long nextLong() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            long res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public int[] nextIntArray(int n) {
            int a[] = new int[n];
            for (int i = 0; i < n; i++)
                a[i] = nextInt();
            return a;
        }
 
        public String readString() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            StringBuilder res = new StringBuilder();
            do {
                res.appendCodePoint(c);
                c = snext();
            } while (!isSpaceChar(c));
            return res.toString();
        }

        public String next() {
            return readString();
        }
 
        public boolean isSpaceChar(int c) {
            if (filter != null)
                return filter.isSpaceChar(c);
            return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
        }
 
        public interface SpaceCharFilter {
            public boolean isSpaceChar(int ch);
        }
    }

}