
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.*;
import java.lang.*;


class BOOKCHEF {
    
    
    
static class FastReader
	{
		final private int BUFFER_SIZE=1<<16;
		private DataInputStream din;
		private byte[] buffer;
		private int bufferPointer,bytesRead;
		
		public FastReader()
		{
			din=new DataInputStream(System.in);
			buffer=new byte[BUFFER_SIZE];
			bufferPointer=bytesRead=0;
		}
		
		public FastReader(String file_name) throws IOException
		{
			din=new DataInputStream(new FileInputStream(file_name));
		    buffer=new byte[BUFFER_SIZE];
		    bufferPointer=bytesRead=0;
		}
		
		private void fillBuffer() throws IOException
		{
			bytesRead =din.read(buffer,bufferPointer=0,BUFFER_SIZE);
		    if(bytesRead==-1)
		    	buffer[0]=-1;
		    
		}
		
		
		
		private byte read() throws IOException
		{
			if (bufferPointer==bytesRead)
				fillBuffer();
			return buffer[bufferPointer++];
		}
		
		public String next() throws IOException
		{
			byte[] buf=new byte[64];
			int cnt=0,c;
			while((c=read())!=-1)
			{
				if(c<=' ')
					break;
				buf[cnt++]=(byte)c; 
			}
			return new String(buf,0,cnt);
		}
		public String nextLine() throws IOException
		{
			byte[] buf=new byte[64];//line length
			int cnt=0,c;
			while((c=read())!=-1)
			{
				if(c=='\n')
					break;
				buf[cnt++]=(byte)c;
			}
			return new String(buf,0,cnt);
		}
		
		public int nextInt() throws IOException
		{
			int ret =0;
			byte c= read();
			while(c<=' ')
				c=read();
			boolean neg=(c=='-');
			if(neg)
				c=read();
			do
			{
				ret=ret*10+c-'0';
			}while((c=read())>='0'&& c<='9');
			if(neg)
				return -ret;
			return ret;
			
		}
		
		public long nextLong() throws IOException
		{
			long ret=0;
			byte c=read();
			while(c<=' ')
				c=read();
			boolean neg=(c=='-');
			if(neg)
				c=read();
			do
			{
				ret=ret*10+c-'0';
			}while((c=read())>='0'&& c<='9');
			if(neg)
				return -ret;
			return ret;
			
		}	
		
		public double nextDouble() throws IOException
		{
			double ret=0,div=1;
			byte c=read();
			while(c<=' ')
				c=read();
			boolean  neg=(c=='-');
			if(neg)
				c=read();
			do{
				ret=ret*10+c-'0';
			}
			while((c=read())>='0' && c<='9');
			
			if(c=='.')
			{
				while((c=read())>='0' && c<='9')
				{
					ret +=(c-'0')/(div*= 10);
				}
			}
			if(neg)
	             return -ret;
			return ret;
		}
	}

public static class DescendingComparator implements Comparator{
   public int compare(Object o1, Object o2) /*descending order*/  {
      if (((Integer) o1).intValue() > ((Integer)o2).intValue()) return -1;
      else if (((Integer) o1).intValue() < ((Integer)o2).intValue()) return 1;
      return 0; /*equal*/
   }
}

 public static void main(String args[]) throws IOException
    {
        FastReader fr= new FastReader();
        PrintWriter out= new PrintWriter(System.out);
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        
        String  lines = br.readLine();    
        String[] strs = lines.split(" ");      
        
        int n=Integer.parseInt(strs[0]);
        int m=Integer.parseInt(strs[1]);
        
        ArrayList<Integer> f= new ArrayList<Integer>();
        ArrayList<Integer> spec= new ArrayList<Integer>();
        ArrayList<Integer> norm= new ArrayList<Integer>();
        
        
        HashMap<Integer,String> hm= new HashMap<>();
        
        String lines2 = br.readLine();
        String[] strs2 = lines2.trim().split(" "); 
        for(int i=0;i<n;i++)
        {
            f.add(Integer.parseInt(strs2[i]));
        }
        
       
        String[] strs3= new String[3];
        
        for(int i=0;i<m;i++)
        {
            String lines3=br.readLine();
            strs3 = lines3.split(" ");
            
            int index=Integer.parseInt(strs3[0]);
            int pop=Integer.parseInt(strs3[1]);
            
            if(f.contains(index))
                spec.add(pop);
            else
                norm.add(pop);
            
            hm.put(pop,strs3[2]);
        }
            
            
            
        Collections.sort(spec, new DescendingComparator());//will sort ArrayList of Integers in descending order
        Collections.sort(norm, new DescendingComparator());//will sort ArrayList of Integers in descending order

            
        for(int j=0;j<spec.size();j++)
         {
               out.println(hm.get(spec.get(j)));
         }
        for(int j=0;j<norm.size();j++)
         {
               out.println(hm.get(norm.get(j)));
         }
        
     
        
        
        out.flush();
        out.close();
    }
        

}
