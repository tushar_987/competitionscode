/**
 *
 * @author Tushar Aug 15, 2016*/
import java.util.Scanner;

class STUDVOTE
{
    private static int count;
    private static int[]arr=new int[105];
    
    
        private static void initialise(int[] arr)
        {
            for(int i=0;i<arr.length;i++)
            {
                arr[i]=0;
            }
        }

	public static void main(String[] arg)
	{
		Scanner sc= new Scanner(System.in);
		int t=sc.nextInt();
		while(t-->0)
                {
                    int n=sc.nextInt();
                    int k=sc.nextInt();
                    count=0;
                    
                    initialise(arr);
                    
                    for(int i=1;i<=n;i++)
                    {
                        int temp=sc.nextInt();
                        if(temp==i)
                            arr[i]=-100;
                        else
                            arr[temp]++;
                        
                    }
                    
                    for(int i=1;i<=n;i++)
                    {
                        if(arr[i]>=k)
                            count++;
                    }
                    System.out.println(count);
                }
		
	
	
	}


}
