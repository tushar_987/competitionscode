//package practice_problems;


import java.io.*;
import java.util.*;
import java.math.*;

/**
 *
 * @author Tushar 3 Aug, 2017
 */
public class C_835 {
    public static void main(String args[])
    {

        //File file= new File("COMPLETE_FILE_PATH");
        //InputStream is = new FileInputStream(file);
        
        
        //InputReader in = new InputReader(is);
        InputReader in = new InputReader(System.in);
        PrintWriter out= new PrintWriter(System.out);
        
        int t=1;
        Task solver=new Task();
        //long start = System.nanoTime();
        solver.solve(t,in,out);
        //long stop = System.nanoTime();
        //System.out.println("Execution time: " + ((stop - start) / 1e+6) + "ms.");
        

        out.flush();
        out.close();


    }

    static class Task {
        long mod = 1000000007;
        private int MAX_CORD=100;
        int n,q,c;
        int star[][][];
        
        public void solve(int testNumber, InputReader in,PrintWriter out){
            n=in.nextInt();
            q=in.nextInt();
            c=in.nextInt();
            
            
            star= new int[c+1][MAX_CORD+1][MAX_CORD+1];
            
            for(int i=0;i<n;i++){
                int x=in.nextInt();
                int y=in.nextInt();
                int s=in.nextInt();
                
                star[s][x][y]++;
            }
            
            for(int i=0;i<=c;i++){
                for(int j=1;j<=MAX_CORD;j++){
                    for(int k=1;k<=MAX_CORD;k++){
                        star[i][j][k]+=star[i][j-1][k]+star[i][j][k-1]-star[i][j-1][k-1];
                    }
                }
            }
            
            
            for(int i=0;i<q;i++){
                int t=in.nextInt();
                int x1=in.nextInt();
                int y1=in.nextInt();
                int x2=in.nextInt();
                int y2=in.nextInt();
                
                out.println(get(t,x1,y1,x2,y2));
            }





        }
        
        
        int get(int t,int x1,int y1,int x2,int y2){
            int result=0;
            for(int i=0;i<=c;i++){
                int bright=(i+t)%(c+1);
                int count=star[i][x2][y2]-star[i][x1-1][y2]-star[i][x2][y1-1]+star[i][x1-1][y1-1];
                result+=count*bright;
            }
            return result;
        }
            
        int LOG2(long item){
            int count=0;
            while(item>1){
                item>>=1;count++;
            }
            return count;
        }

        long power(long x, long n) {
            if (n <= 0) {
                return 1;
            }
            long y = power(x, n / 2);

            if ((n & 1) == 1) {
                return (((y * y) % mod) * x) % mod;
            }

            return (y * y) % mod;
        }

        long gcd(long a, long b) {
            a = Math.abs(a);
            b = Math.abs(b);
            return BigInteger.valueOf(a).gcd(BigInteger.valueOf(b)).longValue();
        }
    }
    
    
    static class InputReader {
 
        private InputStream stream;
        private byte[] buf = new byte[8192];
        private int curChar, snumChars;
        private SpaceCharFilter filter;
 
        public InputReader(InputStream stream) {
            this.stream = stream;
        }
 
        public int snext() {
            if (snumChars == -1)
                throw new InputMismatchException();
            if (curChar >= snumChars) {
                curChar = 0;
                try {
                    snumChars = stream.read(buf);
                } catch (IOException e) {
                    throw new InputMismatchException();
                }
                if (snumChars <= 0)
                    return -1;
            }
            return buf[curChar++];
        }
 
        public int nextInt() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            int res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public long nextLong() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            long res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public int[] nextIntArray(int n) {
            int a[] = new int[n];
            for (int i = 0; i < n; i++)
                a[i] = nextInt();
            return a;
        }
 
        public String readString() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            StringBuilder res = new StringBuilder();
            do {
                res.appendCodePoint(c);
                c = snext();
            } while (!isSpaceChar(c));
            return res.toString();
        }

        public String next() {
            return readString();
        }
 
        public boolean isSpaceChar(int c) {
            if (filter != null)
                return filter.isSpaceChar(c);
            return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
        }
 
        public interface SpaceCharFilter {
            public boolean isSpaceChar(int ch);
        }
    }


}
