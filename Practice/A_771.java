//package practice_problems;

import java.io.*;
import java.util.*;
import java.math.*;

/**
 *
 * @author Tushar Mar 18, 2017
 */
public class A_771 {
    
    private static boolean[] marked;
    private static ArrayList<Integer> sizes;
    
    
    public static void main(String args[])
    {

        //File file= new File("COMPLETE_FILE_PATH");
        //InputStream is = new FileInputStream(file);
        
        
        //InputReader in = new InputReader(is);
        InputReader in = new InputReader(System.in);
        PrintWriter out= new PrintWriter(System.out);
        
        int t=1;
        
        Task solver=new Task();
        solver.solve(t,in,out);
        

        out.flush();
        out.close();


    }

    static class Task {
        long mod = 1000000007;

        public void solve(int testNumber, InputReader in,PrintWriter out){
            int n=in.nextInt();
            int m=in.nextInt();
            HashMap<Integer, ArrayList<Integer>> graph= new HashMap<>();
            
            for(int i=0;i<n;i++){
                graph.put(i, new ArrayList<Integer>());
            }
            
            for(int i=0;i<m;i++){
                int x=in.nextInt()-1;
                int y=in.nextInt()-1;
                graph.get(x).add(y);
                graph.get(y).add(x);                
            }
            int sizeComp=0;
            sizes=new ArrayList<>();
            marked=new boolean[n];
            
            for(int v:graph.keySet()){
                if(!marked[v]){
//                    marked[v]=true;
                    sizeComp=dfs(graph,v);
                    sizes.add(sizeComp);
                }
            }
            
            long sumRequired=0;
            for(long i:sizes){
                sumRequired+=i*(i-1)/2;
            }
            
            if(sumRequired==m)
                out.println("YES");
            else
                out.println("NO");
            

        }
        
        int dfs(HashMap<Integer,ArrayList<Integer>> graph,int v){
            marked[v]=true;
            int count=0;
            count++;
            for(int u:graph.get(v)){
                if(!marked[u]){
                    count+=dfs(graph,u);
                }
            }
            return count;// returning number of vertices in disjoint component
        }
        
            
        int LOG2(long item){
            int count=0;
            while(item>1){
                item>>=1;count++;
            }
            return count;
        }

        long power(long x, long n) {
            if (n <= 0) {
                return 1;
            }
            long y = power(x, n / 2);

            if ((n & 1) == 1) {
                return (((y * y) % mod) * x) % mod;
            }

            return (y * y) % mod;
        }

        long gcd(long a, long b) {
            a = Math.abs(a);
            b = Math.abs(b);
            return BigInteger.valueOf(a).gcd(BigInteger.valueOf(b)).longValue();
        }
    }
    
    
    static class InputReader {
 
        private InputStream stream;
        private byte[] buf = new byte[8192];
        private int curChar, snumChars;
        private SpaceCharFilter filter;
 
        public InputReader(InputStream stream) {
            this.stream = stream;
        }
 
        public int snext() {
            if (snumChars == -1)
                throw new InputMismatchException();
            if (curChar >= snumChars) {
                curChar = 0;
                try {
                    snumChars = stream.read(buf);
                } catch (IOException e) {
                    throw new InputMismatchException();
                }
                if (snumChars <= 0)
                    return -1;
            }
            return buf[curChar++];
        }
 
        public int nextInt() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            int res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public long nextLong() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            long res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public int[] nextIntArray(int n) {
            int a[] = new int[n];
            for (int i = 0; i < n; i++)
                a[i] = nextInt();
            return a;
        }
 
        public String readString() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            StringBuilder res = new StringBuilder();
            do {
                res.appendCodePoint(c);
                c = snext();
            } while (!isSpaceChar(c));
            return res.toString();
        }

        public String next() {
            return readString();
        }
 
        public boolean isSpaceChar(int c) {
            if (filter != null)
                return filter.isSpaceChar(c);
            return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
        }
 
        public interface SpaceCharFilter {
            public boolean isSpaceChar(int ch);
        }
    }

}
