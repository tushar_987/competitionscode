//package practice_problems;

import java.io.*;
import java.util.*;
import java.math.*;

/**
 *
 * @author Tushar Mar 29, 2017
 */
public class dfs_hackerrank {

    public static void main(String args[])
    {

        //File file= new File("COMPLETE_FILE_PATH");
        //InputStream is = new FileInputStream(file);
        
        
        //InputReader in = new InputReader(is);
        InputReader in = new InputReader(System.in);
        PrintWriter out= new PrintWriter(System.out);
        
        int t=in.nextInt();
        
        Task solver=new Task();
        for(int i=0;i<t;i++){
            solver.solve(t,in,out);
        }

        out.flush();
        out.close();


    }

    static class Task {
        long mod = 1000000007;
        public boolean[] visited;
        HashMap<Integer,HashSet<Integer>> edges=new HashMap<>();

        public void solve(int testNumber, InputReader in,PrintWriter out){
            int n,m,c_lib,c_road;
            n=in.nextInt(); m=in.nextInt();
            c_lib=in.nextInt(); c_road=in.nextInt();
            
            
            for(int i=0;i<n;i++){
                edges.put(i, new HashSet<Integer>());
            }
            
            visited=new boolean[n];
            for(int i=0;i<m;i++){
                int u=in.nextInt()-1;int v=in.nextInt()-1;
                edges.get(u).add(v);
                edges.get(v).add(u);
            }
            long road=0,lib=0;
            for(int i=0;i<n;i++){
                if(!visited[i]){
                    road+=Dfs(i)-1;
                    ++lib;
                }
                
            }
            
            out.println(Math.min(road*c_road+lib*c_lib,(long)c_lib*n));
        

        }
        
        int Dfs(int u){
            visited[u]=true;
            int count=1;
            for(int i : edges.get(u)){
                if(!visited[i]){
                    count+=Dfs(i);
                }
            }
            return count;
        }
            
        int LOG2(long item){
            int count=0;
            while(item>1){
                item>>=1;count++;
            }
            return count;
        }

        long power(long x, long n) {
            if (n <= 0) {
                return 1;
            }
            long y = power(x, n / 2);

            if ((n & 1) == 1) {
                return (((y * y) % mod) * x) % mod;
            }

            return (y * y) % mod;
        }

        long gcd(long a, long b) {
            a = Math.abs(a);
            b = Math.abs(b);
            return BigInteger.valueOf(a).gcd(BigInteger.valueOf(b)).longValue();
        }
    }
    
    
    static class InputReader {
 
        private InputStream stream;
        private byte[] buf = new byte[8192];
        private int curChar, snumChars;
        private SpaceCharFilter filter;
 
        public InputReader(InputStream stream) {
            this.stream = stream;
        }
 
        public int snext() {
            if (snumChars == -1)
                throw new InputMismatchException();
            if (curChar >= snumChars) {
                curChar = 0;
                try {
                    snumChars = stream.read(buf);
                } catch (IOException e) {
                    throw new InputMismatchException();
                }
                if (snumChars <= 0)
                    return -1;
            }
            return buf[curChar++];
        }
 
        public int nextInt() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            int res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public long nextLong() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            long res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public int[] nextIntArray(int n) {
            int a[] = new int[n];
            for (int i = 0; i < n; i++)
                a[i] = nextInt();
            return a;
        }
 
        public String readString() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            StringBuilder res = new StringBuilder();
            do {
                res.appendCodePoint(c);
                c = snext();
            } while (!isSpaceChar(c));
            return res.toString();
        }

        public String next() {
            return readString();
        }
 
        public boolean isSpaceChar(int c) {
            if (filter != null)
                return filter.isSpaceChar(c);
            return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
        }
 
        public interface SpaceCharFilter {
            public boolean isSpaceChar(int ch);
        }
    }

}
