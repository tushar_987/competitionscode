//package practice_problems;

import java.io.*;
import java.util.*;
import java.math.*;

/**
 *
 * @author Tushar Feb 25, 2017
 */
class QHOUSE {

    public static void main(String args[])
    {

        //File file= new File("COMPLETE_FILE_PATH");
        //InputStream is = new FileInputStream(file);
        
        
        //InputReader in = new InputReader(is);
        InputReader in = new InputReader(System.in);
        PrintWriter out= new PrintWriter(System.out);
        
        int t=1;
        
        Task solver=new Task();
        solver.solve(t,in,out);
        

        out.flush();
        out.close();


    }

    static class Task {
        long mod = 1000000007;

        public void solve(int testNumber, InputReader in,PrintWriter out){
            
            //Finding y coordiante
            
            int y_lo=1,y_hi=1000;
            int y=1000;
            
            
            while(true){
                out.println("? "+"0 "+y);
                out.flush();
                boolean response=in.next().equals("YES")?true:false;
                if(response){
                    if(y_lo!=y){
                        y_lo=y;
                        y= (int)Math.ceil((double)(y_lo+y_hi)/2);
                    }
                    else break;
                }
                else{
                    y_hi=y;
                    y= (int)Math.floor((double)(y_lo+y_hi)/2);
                }
            }
            //X cOordiante;
            int x_lo=1,x_hi=1000;
            int x=1000;
            
            
            while(true){
                out.println("? "+x+" 0");
                out.flush();
                boolean response=in.next().equals("YES")?true:false;
                if(response){
                    if(x_lo!=x){
                        x_lo=x;
                        x= (int)Math.ceil((double)(x_lo+x_hi)/2);
                    }
                    else break;
                }
                else{
                    x_hi=x;
                    x= (int)Math.floor((double)(x_lo+x_hi)/2);
                }
            }
            
            int t_lo=x,t_hi=1000;
            int t=1000;
            
            
            while(true){
                out.println("? "+t+" "+(2*x));
                out.flush();
                boolean response=in.next().equals("YES")?true:false;
                if(response){
                    if(t_lo!=t){
                        t_lo=t;
                        t= (int)Math.ceil((double)(t_lo+t_hi)/2);
                    }
                    else break;
                }
                else{
                    t_hi=t;
                    t= (int)Math.floor((double)(t_lo+t_hi)/2);
                }
            }
            
            
            int area_sq=(x*2)*(x*2);
            int area_tr=(t*2*(y-(2*x)))/2;
            int ar_home=area_sq+area_tr;
            
            out.println("! "+ar_home);
//            out.println(x);
//            out.println(y);
//            out.println(t);
            
        

        }
            
        int LOG2(long item){
            int count=0;
            while(item>1){
                item>>=1;count++;
            }
            return count;
        }


        long gcd(long a, long b) {
            a = Math.abs(a);
            b = Math.abs(b);
            return BigInteger.valueOf(a).gcd(BigInteger.valueOf(b)).longValue();
        }
    }
    
    
    static class InputReader {
 
        private InputStream stream;
        private byte[] buf = new byte[8192];
        private int curChar, snumChars;
        private SpaceCharFilter filter;
 
        public InputReader(InputStream stream) {
            this.stream = stream;
        }
 
        public int snext() {
            if (snumChars == -1)
                throw new InputMismatchException();
            if (curChar >= snumChars) {
                curChar = 0;
                try {
                    snumChars = stream.read(buf);
                } catch (IOException e) {
                    throw new InputMismatchException();
                }
                if (snumChars <= 0)
                    return -1;
            }
            return buf[curChar++];
        }
 
        public int nextInt() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            int res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public long nextLong() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            long res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public int[] nextIntArray(int n) {
            int a[] = new int[n];
            for (int i = 0; i < n; i++)
                a[i] = nextInt();
            return a;
        }
 
        public String readString() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            StringBuilder res = new StringBuilder();
            do {
                res.appendCodePoint(c);
                c = snext();
            } while (!isSpaceChar(c));
            return res.toString();
        }

        public String next() {
            return readString();
        }
 
        public boolean isSpaceChar(int c) {
            if (filter != null)
                return filter.isSpaceChar(c);
            return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
        }
 
        public interface SpaceCharFilter {
            public boolean isSpaceChar(int ch);
        }
    }

}
