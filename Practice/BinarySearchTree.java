package das_practice_code;

/**
 *
 * @author Tushar Sep 8, 2016
 */
public class BinarySearchTree {
        public static Node root;
        public BinarySearchTree()
        {
                this.root=null;
        }
        
        public boolean find(int key)
        {
            Node current=root;
            while(current!=null)
            {
                if(current.data==key)
                    return true;
                else if(key<current.data)
                    current=current.left;
                else
                    current=current.right;
            }
            return false;
        }
        
        public boolean delete(int key)
        {
            Node current=root;
            Node parent=null;
            boolean isLeftChild=false;
            
            while(current.data!=key)
            {
                parent=current;
                if(key<current.data)
                {
                    isLeftChild=true;
                    current=current.left;
                }
                else
                {
                    isLeftChild=false;
                    current=current.right;
                }
                if(current==null)
                {
                    return false;
                }
            }
            
            //If your code reaches here.. That means
            //The Key Eleemnt was found successfully
            
            //case:1(node to be deletd has no child
            if(current.left==null && current.right==null)
            {
                if(current==root)
                    root=null;
                if(isLeftChild)
                    parent.left=null;
                else
                    parent.right=null;
            }
            
            
            //case2: (node to be deleted has only one child)
            else if(current.left==null)
            {
                if(current==root)
                    root=current.right;
                else if(isLeftChild)
                    parent.left=current.right;
                else
                    parent.right=current.left;
            }
            
            else if(current.right==null)
            {
                if(current==root)
                    root=current.left;
                else if(isLeftChild)
                    parent.left=current.left;
                else
                    parent.right=current.left;
            }
            else
            {
                Node successor=getSuccessor(current);
                if(current==root)
                        root=successor;
                else if(isLeftChild)
                    parent.left=successor;
                else
                    parent.right=successor;
                
            }
            
            return true;
        }
        
        public Node getSuccessor(Node deleteNode)
        {
            Node current=deleteNode.right;
            Node successor=null;
            Node successorParent=null;
            
            while(current!=null)
            {
                successorParent=successor;
                successor=current;
                current=current.left;
            }
            //Check if succesor have a right child ,It can not have a left child
            //if it does have the right child ,add it to the left og=f the successor parent
            if(successor.right!=null)
            {
                successorParent.left=successor.right;
                successor.right=deleteNode.right;
            }
            
            return successor;
                
        }
        
        public void insert(int key)
        {
            Node newNode=new Node(key);
            if(root==null)
            {
                root=newNode;
                return;
            }
            Node current=root;
            Node parent=null;
            while(true)
            {
                parent=current;
                if(key<current.data){
                    current=current.left;
                    if(current==null){
                        parent.left=newNode;
                        return;
                    }
                }
                else
                    current=current.right;
                    if(current==null){
                        parent.right=newNode;
                        return;
                    }
            }
        
        
        }
        
        public void display(Node root)
        {
            if(root!=null){
                display(root.left);
                System.out.print(" "+root.data);
                display(root.right);
            }
        }
        
        
        public static void main(String args[])
        {   
                BinarySearchTree t=new BinarySearchTree();
                t.insert(3);t.insert(8);t.insert(1);t.insert(4);t.insert(6);
                t.insert(2);t.insert(10);t.insert(20);t.insert(25);
                t.insert(15);t.insert(16);t.insert(9);
                
                System.out.print("The Tree is :");
                
                
                t.display(root);
                
                System.out.println("Check whether the node with value 4 "
                        + "exist or not : "+t.find(4));
                System.out.println("Delete node with no children(2)"+t.delete(2));
                t.display(root);
                System.out.println("Delete node with one children(4)"+t.delete(4));
                t.display(root);
                System.out.println("Delete node with two children(10)"+t.delete(10));
                t.display(root);
        }

}


class Node{
        int data;
        Node left;
        Node right;
        
        public Node(int data)
        {
            this.data=data;
            left=null;
            right=null;
        }
}
