//package practice_problems;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.InputMismatchException;

/**
 *
 * @author Tushar Jan 23, 2017
 */
class RESERVOIR {

    long mod = 1000000007;


    public static void main(String args[]) throws FileNotFoundException, IOException
    {
//        
        File file= new File("C:\\Documents\\NetBeansProjects\\Practice_Problems\\src\\practice_problems\\reservoir_input.txt");
        InputStream is = new FileInputStream(file);
        
        
        InputReader in = new InputReader(is);
//        InputReader in = new InputReader(System.in);
        PrintWriter out= new PrintWriter(System.out);

        int t=in.nextInt();
        while(t-->0)
        {
            int n=in.nextInt();
            int m= in.nextInt();
            
            char[][] input= new char[1000][1000];
            String str=null;
            for(int i=0;i<n;i++){
                str= in.next();
                input[i]=str.toCharArray();
                
            }
            boolean stable=true;
            
            
            //stability of row
            for(int i=0;i<n && stable==true;i++){
                for(int j=0;j<m && stable==true;j++){
                    if(input[i][j]=='A'){
                       while(input[i][j]=='A' && j<m-1)
                            ++j;
                        if(input[i][j]=='W' || input[i][j]=='A')
                            stable=false;
                    }
                    else if(input[i][j]=='W'){
                        
                        while(input[i][j]=='W' && j<m-1)
                            ++j;
                        if(input[i][j]=='A' || input[i][j]=='W')
                            stable=false;
                    }
                }
            }
            
            //Stability of Coloumn
            
            for(int i=0;i<m && stable==true;i++){
                int j;
                for(j=0;j<n && stable==true;j++){
                    if(input[j][i]=='B'){
                       while(input[j][i]=='B' && j<n-1)
                            ++j;
                        if(input[j][i]=='W' || input[j][i]=='A')
                            stable=false;
                    }
                    else if(input[j][i]=='W'){
                        
                        while(input[j][i]=='W' && j<n-1)
                            ++j;
                        if(input[j][i]=='A' || input[j][i]=='W')
                            stable=false;
                    }
                    
                }
                if(input[n-1][i]=='A')
                    stable=false;
            }
            
            
//            //Stability of Water
//            for(int i=0;i<m;i++){
//                int j;
//                for(j=0;j<n;j++){
//                    if(input[j][i]=='W'){
//                        while(input[j][i]=='W' && j<n){
//                            ++j;
//                        }
//                        if(input[j][i]=='B' && j<n){
//                            while(input[j][i]=='B' && j<n){
//                            ++j;
//                            }
//                        }
//                        if(j<n){
//                            ans=false;
//                            break;
//                        }
//                        else
//                            ans=true;
//                    }
//                
//                }
//            }
            
//            //Stability of Bricks
//            for(int i=0;i<m;i++){
//                int j;
//                for(j=0;j<n;j++){
//                    if(input[j][i]=='B'){
//                        while(input[j][i]=='B' && j<n){
//                            ++j;
//                        }
//                        if(j<n){
//                            ans=false;
//                            break;
//                        }
//                        else
//                            ans=true;
//                    }
//                }
//                if(j<n)
//                    break;
//            }
            
            
            
        out.println(stable?"yes":"no");
                
        }

        out.flush();
        out.close();


    }



    long power(long x, long n) {
        if (n <= 0) {
            return 1;
        }
        long y = power(x, n / 2);
 
        if ((n & 1) == 1) {
            return (((y * y) % mod) * x) % mod;
        }
 
        return (y * y) % mod;
    }
 
    public long gcd(long a, long b) {
        a = Math.abs(a);
        b = Math.abs(b);
        return BigInteger.valueOf(a).gcd(BigInteger.valueOf(b)).longValue();
    }
    
    
    static class InputReader {
 
        private InputStream stream;
        private byte[] buf = new byte[8192];
        private int curChar, snumChars;
        private SpaceCharFilter filter;
 
        public InputReader(InputStream stream) {
            this.stream = stream;
        }
 
        public int snext() {
            if (snumChars == -1)
                throw new InputMismatchException();
            if (curChar >= snumChars) {
                curChar = 0;
                try {
                    snumChars = stream.read(buf);
                } catch (IOException e) {
                    throw new InputMismatchException();
                }
                if (snumChars <= 0)
                    return -1;
            }
            return buf[curChar++];
        }
 
        public int nextInt() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            int res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public long nextLong() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            long res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public int[] nextIntArray(int n) {
            int a[] = new int[n];
            for (int i = 0; i < n; i++)
                a[i] = nextInt();
            return a;
        }
 
        public String readString() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            StringBuilder res = new StringBuilder();
            do {
                res.appendCodePoint(c);
                c = snext();
            } while (!isSpaceChar(c));
            return res.toString();
        }

        public String next() {
            return readString();
        }
 
        public boolean isSpaceChar(int c) {
            if (filter != null)
                return filter.isSpaceChar(c);
            return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
        }
 
        public interface SpaceCharFilter {
            public boolean isSpaceChar(int ch);
        }
    }

}
