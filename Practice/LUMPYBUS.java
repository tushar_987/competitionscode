import java.util.*;
import java.io.*;

/**
 *
 * @author Tushar Aug 19, 2016
 */
class LUMPYBUS {
    
    static BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
    static FastScanner sc= new FastScanner(br);
    
    static PrintWriter out=new PrintWriter(System.out);
    
    
    public static void main(String args[]) throws Exception
    {
        int t=sc.nextInt();
        while(t>0)
        {
            int n=sc.nextInt();long p=sc.nextLong(),q=sc.nextLong();
            long[] arr=new long[n];
            for(int i=0;i<n;i++)
            {
                arr[i]=sc.nextLong();
            }
            Arrays.sort(arr);
            long res=0;
            
            for(int i=0;i<n;i++)
            {
                long req1=arr[i]%2;
                long req2=arr[i]/2L;
                if(req1<=p && req2<=q)
                {
                    res++;p-=req1;q-=req2;
                }
                else if(q<req2)
                {
                    long dif=(req2-q)*2L;
                    if(p>=dif+req1)
                    {
                        res++;q=0;p-=(req1+dif);
                    }
                }
            }
            out.println(res);
            t--;
        }
        out.close();
    }

}

class FastScanner
{
    BufferedReader in;
    StringTokenizer st;
 
    public FastScanner(BufferedReader in) {
        this.in = in;
    }
	
    public String nextToken() throws Exception {
        while (st == null || !st.hasMoreTokens()) {
            st = new StringTokenizer(in.readLine());
        }
        return st.nextToken();
    }
	
	public String next() throws Exception {
		return nextToken().toString();
	}
	
    public int nextInt() throws Exception {
        return Integer.parseInt(nextToken());
    }
 
    public long nextLong() throws Exception {
        return Long.parseLong(nextToken());
    }
 
    public double nextDouble() throws Exception {
        return Double.parseDouble(nextToken());
    }
}
