import java.io.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Set;
class code{
	 public static void main(String args[]) throws NumberFormatException, IOException{
		   BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	          long t=Long.parseLong(br.readLine());
	         while(t-->0){
	        	int n=Integer.parseInt(br.readLine());
	        	long[] a=new long [n];
	        	 String line[]=br.readLine().split(" ");
	        	 for (int i = 0; i < n; i++)
	        		 a[i]=Integer.parseInt(line[i]);
              long count=0;
	      	 for (int i = 0; i < n-1; i++){
	      		 if(Math.abs(a[i]-a[i+1])<=1)
	      			 count++;
	      	 }
		       if(count==(n-1))
		        System.out.println("YES");	
		       else
		    	System.out.println("NO");	  
	}
}
}