//package practice_problems;

import java.io.*;
import java.util.*;
import java.math.*;

/**
 *
 * @author Tushar Mar 28, 2017
 */
public class LTIME46_SPECIES {

    public static void main(String args[])
    {

        //File file= new File("COMPLETE_FILE_PATH");
        //InputStream is = new FileInputStream(file);
        
        
        //InputReader in = new InputReader(is);
        InputReader in = new InputReader(System.in);
        PrintWriter out= new PrintWriter(System.out);
        
        int t=in.nextInt();
        
        Task solver=new Task();
        for(int i=0;i<t;i++)
            solver.solve(i,in,out);
        

        out.flush();
        out.close();


    }

    static class Task {
        long mod = 1000000007;

        public void solve(int testNumber, InputReader in,PrintWriter out){
            int n=in.nextInt();
            char[][] arr=new char[n][n];
            for(int i=0;i<n;i++){
                String str=in.next();
                arr[i]=str.toCharArray();
            }
//            Printing the Input
//            for(int i=0;i<n;i++){
//                for(int j=0;j<n;j++)
//                    out.print(arr[i][j]);
//                out.println();
//            }
            out.println(isValid(arr, n));
//            ##SUBTASK 1
            
            
            
        }
        
        boolean isValid(char[][] arg,int size){
            for(int i=0;i<size;i++){
                for(int j=0;j<size;j++){
                    switch(arg[i][j]){
                        case 'G':
                            if((i-1)>=0 && arg[i-1][j]!='.' )
                                return false;
                            if((i+1)<size && arg[i+1][j]!='.')
                                return false;
                            if((j-1)>=0 && arg[i][j-1]!='.')
                                return false;
                            if((j+1)<size && arg[i][j+1]!='.')
                                return false;
                            break;
                        case 'B':
                            if((i-1)>=0 && (arg[i-1][j]!='B' &&  arg[i-1][j]!='.' ))
                                return false;
                            if((i+1)<size && arg[i+1][j]!='.' && arg[i+1][j]!='B')
                                return false;
                            if((j-1)>=0 && arg[i][j-1]!='.' && arg[i][j-1]!='B')
                                return false;
                            if((j+1)<size && arg[i][j+1]!='.' && arg[i][j+1]!='B')
                                return false;
                            break;
                        case 'P':
                            if((i-1)>=0 && (arg[i-1][j]!='P' &&  arg[i-1][j]!='.' ))
                                return false;
                            if((i+1)<size && arg[i+1][j]!='.' && arg[i+1][j]!='P')
                                return false;
                            if((j-1)>=0 && arg[i][j-1]!='.' && arg[i][j-1]!='P')
                                return false;
                            if((j+1)<size && arg[i][j+1]!='.' && arg[i][j+1]!='P')
                                return false;
                            break;
                        default:
                            break;
                    }
                        
                            
                }
            }
            return true;
        }
            
        int LOG2(long item){
            int count=0;
            while(item>1){
                item>>=1;count++;
            }
            return count;
        }

        long power(long x, long n) {
            if (n <= 0) {
                return 1;
            }
            long y = power(x, n / 2);

            if ((n & 1) == 1) {
                return (((y * y) % mod) * x) % mod;
            }

            return (y * y) % mod;
        }

        long gcd(long a, long b) {
            a = Math.abs(a);
            b = Math.abs(b);
            return BigInteger.valueOf(a).gcd(BigInteger.valueOf(b)).longValue();
        }
    }
    
    
    static class InputReader {
 
        private InputStream stream;
        private byte[] buf = new byte[8192];
        private int curChar, snumChars;
        private SpaceCharFilter filter;
 
        public InputReader(InputStream stream) {
            this.stream = stream;
        }
 
        public int snext() {
            if (snumChars == -1)
                throw new InputMismatchException();
            if (curChar >= snumChars) {
                curChar = 0;
                try {
                    snumChars = stream.read(buf);
                } catch (IOException e) {
                    throw new InputMismatchException();
                }
                if (snumChars <= 0)
                    return -1;
            }
            return buf[curChar++];
        }
 
        public int nextInt() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            int res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public long nextLong() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            long res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public int[] nextIntArray(int n) {
            int a[] = new int[n];
            for (int i = 0; i < n; i++)
                a[i] = nextInt();
            return a;
        }
 
        public String readString() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            StringBuilder res = new StringBuilder();
            do {
                res.appendCodePoint(c);
                c = snext();
            } while (!isSpaceChar(c));
            return res.toString();
        }

        public String next() {
            return readString();
        }
 
        public boolean isSpaceChar(int c) {
            if (filter != null)
                return filter.isSpaceChar(c);
            return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
        }
 
        public interface SpaceCharFilter {
            public boolean isSpaceChar(int ch);
        }
    }

}
