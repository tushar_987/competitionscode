//package practice_problems;

import java.io.*;
import java.util.*;
import java.math.*;

/**
 *
 * @author Tushar Feb 20, 2017
 */
public class Tree{
    
    static long count=0;
    static long one=0;
    
    class Node{
        long key;
        Node left,right;
        public Node(long item){
            left=right=null;
            key=item;
        }
    }
    
    
        static Node root;
        
        Tree(){
            root=null;
        }
        
        Node makeTree(long item){
            long n=item%2;
            if(root==null){
                root= new Node(n);
                if(item!=0 && item!=1){
                    root.left=makeTree(item/2);
                    root.right=makeTree(item/2);
                 }
                return root;
            }
            else{
                Node node= new Node(n);
                if(item!=1 && item!=0){
                    node.left=makeTree(item/2);
                    node.right=makeTree(item/2);
                }
                
                return node;
            }
        }
        
        void inorder(Node m,long left,long right){
            if(m!=null){
                inorder(m.left,left,right);
                count++;
                if(count>=left && count<=right && m.key==1)
                    ++one;
                inorder(m.right,left,right);
                    
                
            }
        }

    long mod = 1000000007;


    public static void main(String args[])
    {

        //File file= new File("COMPLETE_FILE_PATH");
        //InputStream is = new FileInputStream(file);
        
        
        //InputReader in = new InputReader(is);
        InputReader in = new InputReader(System.in);
        PrintWriter out= new PrintWriter(System.out);

        int t=1;
        while(t-->0)
        {
            long n=in.nextInt();
            long l=in.nextInt();
            long r= in.nextInt();
            
            Tree tree= new Tree();
            tree.makeTree(n);
            
            long temp=1;
            while(n!=0 && n!=1){
                n/=2;
                ++temp;
            }
            long totalNodes=tree.power(2, temp)-1;
            long middle=totalNodes/2+1;
            
            if(l<middle && r>middle){
                tree.inorder(root, l, r);
                
                out.println(one);
            }
            else if(l<middle && r<middle){
                tree.inorder(root, l, r);
                out.println(one);
            }
            else{
                l=l-middle;
                r=r-middle;
                tree.inorder(root, r, l);
                out.println(one);
                
            }
            
            
            
            

        }

        out.flush();
        out.close();


    }



    public long power(long x, long n) {
        if (n <= 0) {
            return 1;
        }
        long y = power(x, n / 2);
 
        if ((n & 1) == 1) {
            return (((y * y) % mod) * x) % mod;
        }
 
        return (y * y) % mod;
    }
 
    public long gcd(long a, long b) {
        a = Math.abs(a);
        b = Math.abs(b);
        return BigInteger.valueOf(a).gcd(BigInteger.valueOf(b)).longValue();
    }
    
    
    static class InputReader {
 
        private InputStream stream;
        private byte[] buf = new byte[8192];
        private int curChar, snumChars;
        private SpaceCharFilter filter;
 
        public InputReader(InputStream stream) {
            this.stream = stream;
        }
 
        public int snext() {
            if (snumChars == -1)
                throw new InputMismatchException();
            if (curChar >= snumChars) {
                curChar = 0;
                try {
                    snumChars = stream.read(buf);
                } catch (IOException e) {
                    throw new InputMismatchException();
                }
                if (snumChars <= 0)
                    return -1;
            }
            return buf[curChar++];
        }
 
        public int nextInt() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            int res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public long nextLong() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            long res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public int[] nextIntArray(int n) {
            int a[] = new int[n];
            for (int i = 0; i < n; i++)
                a[i] = nextInt();
            return a;
        }
 
        public String readString() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            StringBuilder res = new StringBuilder();
            do {
                res.appendCodePoint(c);
                c = snext();
            } while (!isSpaceChar(c));
            return res.toString();
        }

        public String next() {
            return readString();
        }
 
        public boolean isSpaceChar(int c) {
            if (filter != null)
                return filter.isSpaceChar(c);
            return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
        }
 
        public interface SpaceCharFilter {
            public boolean isSpaceChar(int ch);
        }
    }

}
