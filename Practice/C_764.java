//package practice_problems;

import java.io.*;
import java.util.*;
import java.math.*;

/**
 *
 * @author Tushar Feb 15, 2017
 */



public class C_764 {

    long mod = 1000000007;
    
    private static int V;
    private static LinkedList<Integer>[] adj;
    private static int[] color;
    
    
    
    C_764(int v){
        V=v;
        adj= new LinkedList[V+1];
        for(int i=1;i<=V;i++){
            adj[i]= new LinkedList<>();
        }
        
        color= new int[V+1];
    }
    
    void addEdge(int u,int v){
        adj[u].add(v);
        adj[v].add(u);
    }
    
    void addColor(int[] c){
        color=c;
    }
    
    boolean dfs(int v){
        boolean[] visited= new boolean[V+1];
        
        visited[v]= true;
        
        Iterator<Integer> i= adj[v].listIterator();
        boolean temp=true;
        while(i.hasNext()){
            int n= i.next();
            int Color_n=color[n];
            visited[n]=true;
            temp=dfsUtil(n, visited,Color_n);
        }
        
        return temp;
    }
    
    
    boolean dfsUtil(int v,boolean visited[],int vertex_color){
        visited[v]=true;
        
        if(color[v]!=vertex_color)
            return false;
        
        Iterator<Integer> i= adj[v].listIterator();
        boolean temp=true;
        while(i.hasNext()){
            int n=i.next();
            if(visited[n]==false){
                temp=dfsUtil(n, visited,vertex_color);
            }
        }
        return temp;
    }
    
    boolean checkColor(int v){
        boolean ret=dfs(v);
        return ret;
    }
    
    
    public static void main(String args[])
    {

        //File file= new File("COMPLETE_FILE_PATH");
        //InputStream is = new FileInputStream(file);
        
        
        //InputReader in = new InputReader(is);
        InputReader in = new InputReader(System.in);
        PrintWriter out= new PrintWriter(System.out);

        int t=1;
        while(t-->0)
        {
            int n=in.nextInt();
            C_764 graph=new C_764(n);
            
            for(int i=1;i<=n-1;i++){
                int a=in.nextInt();
                int b= in.nextInt();
                graph.addEdge(a, b);//add Edge
            }
            
            int[] c= new int[n+1];
            for(int i=1;i<=n;i++){
                c[i]=in.nextInt();
            }
            
            graph.addColor(c);
            
            //get a different colored vertices
            int m=1;
            int j=1;
            for(j=1;j<=V;j++){
                Iterator<Integer> i= adj[j].listIterator();
                while(i.hasNext()){
                    m=i.next();
                    if(color[m]!=color[j])
                        break;
                }
                if(color[m]!=color[j])
                        break;
            }
            
            if(j==V+1){//all vertices in same color
                out.println("YES");
                out.println(V);
            }
            else{
                boolean check=false;
                
                check=graph.checkColor(j);
                if(check){
                    out.println("YES"+"\n"+j);
                }
                else{
                    check=graph.checkColor(m);
                    if(check){
                        out.println("YES"+"\n"+m);
                    }
                    else
                        out.println("NO");
                }
                
            }
        }

        out.flush();
        out.close();


    }



    long power(long x, long n) {
        if (n <= 0) {
            return 1;
        }
        long y = power(x, n / 2);
 
        if ((n & 1) == 1) {
            return (((y * y) % mod) * x) % mod;
        }
 
        return (y * y) % mod;
    }
 
    public long gcd(long a, long b) {
        a = Math.abs(a);
        b = Math.abs(b);
        return BigInteger.valueOf(a).gcd(BigInteger.valueOf(b)).longValue();
    }
    
    
    static class InputReader {
 
        private InputStream stream;
        private byte[] buf = new byte[8192];
        private int curChar, snumChars;
        private SpaceCharFilter filter;
 
        public InputReader(InputStream stream) {
            this.stream = stream;
        }
 
        public int snext() {
            if (snumChars == -1)
                throw new InputMismatchException();
            if (curChar >= snumChars) {
                curChar = 0;
                try {
                    snumChars = stream.read(buf);
                } catch (IOException e) {
                    throw new InputMismatchException();
                }
                if (snumChars <= 0)
                    return -1;
            }
            return buf[curChar++];
        }
 
        public int nextInt() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            int res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public long nextLong() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            long res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public int[] nextIntArray(int n) {
            int a[] = new int[n];
            for (int i = 0; i < n; i++)
                a[i] = nextInt();
            return a;
        }
 
        public String readString() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            StringBuilder res = new StringBuilder();
            do {
                res.appendCodePoint(c);
                c = snext();
            } while (!isSpaceChar(c));
            return res.toString();
        }

        public String next() {
            return readString();
        }
 
        public boolean isSpaceChar(int c) {
            if (filter != null)
                return filter.isSpaceChar(c);
            return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
        }
 
        public interface SpaceCharFilter {
            public boolean isSpaceChar(int ch);
        }
    }

}
