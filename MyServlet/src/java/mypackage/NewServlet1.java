/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mypackage;

import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.Math.sqrt;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author This Pc
 */
public class NewServlet1 extends HttpServlet {

   public void doPost(HttpServletRequest req,HttpServletResponse res)throws IOException,ServletException
   {
     res.setContentType("text/html");
     PrintWriter out=res.getWriter();
     
     int a=Integer.parseInt(req.getParameter("a"));
     int b=Integer.parseInt(req.getParameter("b"));
     int c=Integer.parseInt(req.getParameter("c"));
     
     double f1,f2,re,disc;
     disc=(b*b-4*a*c);
     re=-b/2*a;

     f1=re+sqrt(disc)/(2*a);
     f2=re-sqrt(disc)/(2*a);
     
     
     String s=new String("<HTML>"
             + "<head><title>Servlet Response</title></head>"
             + "<body style=\"background-color:lightgrey;text-align:center\" >"
             + "a="+a+"b="+b+"c="+c
             + "<p>First Root is <b>"+f1+"</b>"
             + "<p>Second Root is <b>"+f2+"</b>"
             + "</body></html>");
     
     out.print(s);
     out.close();
   }
}