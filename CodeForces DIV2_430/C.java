import java.io.*;
import java.util.*;
import java.math.*;

//package linux_coding;

/**
 *
 * @author ghost
 */
public class C {

 
    public static void main(String args[])
    {
 
        //File file= new File("COMPLETE_FILE_PATH");
        //InputStream is = new FileInputStream(file);
        
        
        //InputReader in = new InputReader(is);
        InputReader in = new InputReader(System.in);
        PrintWriter out= new PrintWriter(System.out);
        
        int t=1;
        
        Task solver=new Task();
        solver.solve(t,in,out);
        
 
        out.flush();
        out.close();
 
 
    }
 
    static class Task {
        long mod = 1000000007;
        boolean visited[];
        Map<Integer,ArrayList<Integer>> adj;
        int[] ans;int ans2[];int[] arr;
        List<Integer> divisors;
        List<Integer> div_num;
 
        public void solve(int testNumber, InputReader in,PrintWriter out){
            int n= in.nextInt();
            ans= new int[n];
            ans2= new int[n];
            arr= new int[n];
            arr= in.nextIntArray(n);
            
            visited= new boolean[n];
            adj= new HashMap<>(n);  
            for(int i=0;i<n;i++)    adj.put(i, new ArrayList<Integer>());
            
            for(int i=0;i<n-1;i++){
                int x=in.nextInt()-1;
                int y= in.nextInt()-1;
                adj.get(x).add(y);
                adj.get(y).add(x);
                
            }

            ans2[0]=0;
            Arrays.fill(visited,false);
            divisors= new ArrayList<>();
            for(int i=1;i*i<=arr[0];i++){
                if(arr[0]%i==0){
                    divisors.add(i);
                    if(arr[0]/i!=i) divisors.add(arr[0]/i);
                }
            }
            Collections.sort(divisors);
//           System.out.println(divisors);
            
            div_num= new ArrayList<>(divisors.size());
            
            for(int i=0;i<divisors.size();i++)   div_num.add(0);
            dfs2(0,0);
            for(int i=0;i<n;i++){
                out.println(Math.max(ans[i],ans2[i])+" ");
            }
            out.println();
            
            
        }
        
        void dfs2(int node,int dist){
            visited[node]=true;
            for(int i=0;i<divisors.size();i++){
                int div=divisors.get(i);
                if((arr[node]%div)==0)  
                    div_num.set(i,div_num.get(i)+1);
                if(div_num.get(i)>=dist)  ans[node]=div;
            }
            for(int i:adj.get(node)){
                if(!visited[i]) {
                    ans2[i]=gcd(ans2[node],arr[i]);
                    dfs2(i,dist+1);
                    
                }
            }
            for(int i=0;i<divisors.size();i++){
                if(arr[node]%divisors.get(i)==0)div_num.set(i,div_num.get(i)-1);
            }
            
        }
            
        int LOG2(long item){
            int count=0;
            while(item>1){
                item>>=1;count++;
            }
            return count;
        }
 
 
        int gcd(int a, int b) {
            if(b==0)    return a;
            return gcd(b,a%b);
        }
    }
    
    
    static class InputReader {
 
        private InputStream stream;
        private byte[] buf = new byte[8192];
        private int curChar, snumChars;
        private SpaceCharFilter filter;
 
        public InputReader(InputStream stream) {
            this.stream = stream;
        }
 
        public int snext() {
            if (snumChars == -1)
                throw new InputMismatchException();
            if (curChar >= snumChars) {
                curChar = 0;
                try {
                    snumChars = stream.read(buf);
                } catch (IOException e) {
                    throw new InputMismatchException();
                }
                if (snumChars <= 0)
                    return -1;
            }
            return buf[curChar++];
        }
 
        public int nextInt() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            int res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public long nextLong() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            long res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public int[] nextIntArray(int n) {
            int a[] = new int[n];
            for (int i = 0; i < n; i++)
                a[i] = nextInt();
            return a;
        }
 
        public String readString() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            StringBuilder res = new StringBuilder();
            do {
                res.appendCodePoint(c);
                c = snext();
            } while (!isSpaceChar(c));
            return res.toString();
        }
 
        public String next() {
            return readString();
        }
 
        public boolean isSpaceChar(int c) {
            if (filter != null)
                return filter.isSpaceChar(c);
            return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
        }
 
        public interface SpaceCharFilter {
            public boolean isSpaceChar(int ch);
        }

        private static void pa(Object... o) {
            System.out.println(Arrays.deepToString(o));
        }
    }

}