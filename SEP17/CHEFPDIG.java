import java.io.*;
import java.util.*;
import java.math.*;

//package SEP17;

/**
 *
 * @author ghost
 */
class CHEFPDIG {

 
    public static void main(String args[])
    {
 
        //File file= new File("COMPLETE_FILE_PATH");
        //InputStream is = new FileInputStream(file);
        
        
        //InputReader in = new InputReader(is);
        InputReader in = new InputReader(System.in);
        PrintWriter out= new PrintWriter(System.out);
        
        int t=in.nextInt();
        while(t-->0){
            Task solver=new Task();
            solver.solve(t,in,out);
        }
 
        out.flush();
        out.close();
 
 
    }
 
    static class Task {
        long mod = 1000000007;
 
        public void solve(int testNumber, InputReader in,PrintWriter out){
            char[] number= in.next().toCharArray();
            int[] frequency= new int[10];
            
            for(int i=0;i<number.length;i++)    frequency[number[i]-'0']++;
            
            
            for(int i=6;i<9;i++){
                for(int j=0;j<10;j++){
                    if(i==6 && j<5) continue;
                    if(i==j){
                        if(frequency[i]>=2) out.print((char)(i*10+i));
                    }
                    else if(frequency[i]>=1 && frequency[j]>=1) out.print((char)(i*10+j));
                }
            }
            if(frequency[9]>=1 && frequency[0]>=1)  out.print((char)(9*10));
            out.println();
        }
            
        int LOG2(long item){
            int count=0;
            while(item>1){
                item>>=1;count++;
            }
            return count;
        }
 
 
        long gcd(long a, long b) {
            a = Math.abs(a);
            b = Math.abs(b);
            return BigInteger.valueOf(a).gcd(BigInteger.valueOf(b)).longValue();
        }
    }
    
    
    static class InputReader {
 
        private InputStream stream;
        private byte[] buf = new byte[8192];
        private int curChar, snumChars;
        private SpaceCharFilter filter;
 
        public InputReader(InputStream stream) {
            this.stream = stream;
        }
 
        public int snext() {
            if (snumChars == -1)
                throw new InputMismatchException();
            if (curChar >= snumChars) {
                curChar = 0;
                try {
                    snumChars = stream.read(buf);
                } catch (IOException e) {
                    throw new InputMismatchException();
                }
                if (snumChars <= 0)
                    return -1;
            }
            return buf[curChar++];
        }
 
        public int nextInt() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            int res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public long nextLong() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            long res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public int[] nextIntArray(int n) {
            int a[] = new int[n];
            for (int i = 0; i < n; i++)
                a[i] = nextInt();
            return a;
        }
 
        public String readString() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            StringBuilder res = new StringBuilder();
            do {
                res.appendCodePoint(c);
                c = snext();
            } while (!isSpaceChar(c));
            return res.toString();
        }
 
        public String next() {
            return readString();
        }
 
        public boolean isSpaceChar(int c) {
            if (filter != null)
                return filter.isSpaceChar(c);
            return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
        }
 
        public interface SpaceCharFilter {
            public boolean isSpaceChar(int ch);
        }

        private static void pa(Object... o) {
            System.out.println(Arrays.deepToString(o));
        }
    }

}
