//package SEP17;


import java.io.*;
import java.util.*;
import java.math.*;

/**
 *
 * @author Tushar 4 Sep, 2017
 */
class SEACO {
    public static void main(String args[])
    {

        //File file= new File("COMPLETE_FILE_PATH");
        //InputStream is = new FileInputStream(file);
        
        
        //InputReader in = new InputReader(is);
        InputReader in = new InputReader(System.in);
        PrintWriter out= new PrintWriter(System.out);
        
        int t=in.nextInt();
        while(t-->0)
        {   Task solver=new Task();
            //long start = System.nanoTime();
            solver.solve(t,in,out);
            //long stop = System.nanoTime();
            //System.out.println("Execution time: " + ((stop - start) / 1e+6) + "ms.");
        }

        out.flush();
        out.close();


    }

    static class Task {
        int mod = 1000000007;

        public void solve(int testNumber, InputReader in,PrintWriter out){
            int n=in.nextInt();
            int m= in.nextInt();
            
            
            List<ArrayList<Integer>> list= new ArrayList<>();
            int[] f= new int[m];
            boolean[] type=new boolean[m];
            
            for(int i=0;i<m;i++){
                int t= in.nextInt();
                int l= in.nextInt()-1;
                int r= in.nextInt()-1;
                if(t==2)    type[i]=true;
                list.add(new ArrayList<Integer>(2));
                list.get(i).add(l); //Left  
                list.get(i).add(r); //Right
                f[i]=1;//frequency
            }
            
            for(int i=m-1;i>=0;i--){
                if(type[i]){    
                    int l=list.get(i).get(0);
                    int r=list.get(i).get(1);
                    int freq=f[i];
                    for(int j=l;j<=r;j++){
                        f[j]=(f[j]+freq%mod)%mod;
                    }
                    
                }
            }
                       
            long[] prefix_sum= new long[n+1];
            
            for(int i=0;i<m;i++){
                if(!type[i]){
                    int l=list.get(i).get(0);
                    int r=list.get(i).get(1);
                    int freq=f[i];
                    
                    prefix_sum[l]=(prefix_sum[l]+freq)%mod;
                    prefix_sum[r+1]=(prefix_sum[r+1]-freq)%mod;                    
                }
            }
            
            prefix_sum[0]=prefix_sum[0]%mod;
            out.print(prefix_sum[0]+" ");
            for(int i=1;i<n;i++)   {
                prefix_sum[i]=(prefix_sum[i]+prefix_sum[i-1])%mod;
                out.print(prefix_sum[i]+" ");
            }
            
            
            out.println();

        }
            
        int LOG2(long item){
            int count=0;
            while(item>1){
                item>>=1;count++;
            }
            return count;
        }

        long power(long x, long n) {
            if (n <= 0) {
                return 1;
            }
            long y = power(x, n / 2);

            if ((n & 1) == 1) {
                return (((y * y) % mod) * x) % mod;
            }

            return (y * y) % mod;
        }

        long gcd(long a, long b) {
            a = Math.abs(a);
            b = Math.abs(b);
            return BigInteger.valueOf(a).gcd(BigInteger.valueOf(b)).longValue();
        }
    }
    
    
    static class InputReader {
 
        private InputStream stream;
        private byte[] buf = new byte[8192];
        private int curChar, snumChars;
        private SpaceCharFilter filter;
 
        public InputReader(InputStream stream) {
            this.stream = stream;
        }
 
        public int snext() {
            if (snumChars == -1)
                throw new InputMismatchException();
            if (curChar >= snumChars) {
                curChar = 0;
                try {
                    snumChars = stream.read(buf);
                } catch (IOException e) {
                    throw new InputMismatchException();
                }
                if (snumChars <= 0)
                    return -1;
            }
            return buf[curChar++];
        }
 
        public int nextInt() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            int res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public long nextLong() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            long res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public int[] nextIntArray(int n) {
            int a[] = new int[n];
            for (int i = 0; i < n; i++)
                a[i] = nextInt();
            return a;
        }
 
        public String readString() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            StringBuilder res = new StringBuilder();
            do {
                res.appendCodePoint(c);
                c = snext();
            } while (!isSpaceChar(c));
            return res.toString();
        }

        public String next() {
            return readString();
        }
 
        public boolean isSpaceChar(int c) {
            if (filter != null)
                return filter.isSpaceChar(c);
            return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
        }
 
        public interface SpaceCharFilter {
            public boolean isSpaceChar(int ch);
        }
    }


}
