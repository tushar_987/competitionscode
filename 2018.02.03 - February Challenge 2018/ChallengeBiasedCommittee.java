package contest;

import FastIO.InputReader;
import FastIO.OutputWriter;

import java.util.Random;

public class ChallengeBiasedCommittee {
    public void solve(int testNumber, InputReader in, OutputWriter out) {
        int n= in.nextInt();
        int m= in.nextInt();
        int[][] arr = new int[m][2];
        for (int i = 0; i < m; i++) {
            arr[i][0]=in.nextInt();
            arr[i][1]= in.nextInt();
        }

        for (int i = 0; i < n; i++) {
            int[] tmp= in.nextIntArray(m);
        }

        Random rand= new Random();
        for (int i = 0; i < m; i++) {
            out.print(rand.nextInt(arr[i][1]-arr[i][0])+arr[i][0] + " ");
        }
        out.println();
    }
}
