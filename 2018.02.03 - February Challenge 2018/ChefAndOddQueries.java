package contest;

import FastIO.InputReader;
import FastIO.OutputWriter;

public class ChefAndOddQueries {
    public void solve(int testNumber, InputReader in, OutputWriter out) {
        int n=in.nextInt();
        int[][] arr= new int[n][2];
        for (int i = 0; i < n; i++) {
            arr[i][0]= in.nextInt();
            arr[i][1]= in.nextInt();
        }

        int q= in.nextInt();
        while(q-->0){
            int m= in.nextInt();
            int[] numbers= in.nextIntArray(m);
            int sum=0;
            for (int i = 0; i < n; i++) {
                int count=0;
                for(int num:numbers){
                    if(num<= arr[i][1] && num>= arr[i][0])  count++;
                }
                if(count%2!=0)  sum++;
            }
            out.println(sum);
        }
    }
}
