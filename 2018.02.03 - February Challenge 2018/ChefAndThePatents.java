package contest;

import FastIO.InputReader;
import FastIO.OutputWriter;

public class ChefAndThePatents {
    public void solve(int testNumber, InputReader in, OutputWriter out) {
        int n = in.nextInt();
        int m = in.nextInt();
        int x = in.nextInt();
        int k = in.nextInt();
        int even = 0;
        int odd = 0;
        char[] ch = in.next().toCharArray();
        for (int i = 0; i < ch.length; i++) {
            if (ch[i] == 'E') even++;
            else odd++;
        }

        if (n > m*x || n > k) {
            out.println("no");
            return;
        }

        boolean flag = false;
        int count = 1;
        while (n > 0 && count <= m) {
            flag ^= true;
            count++;
            if (flag) {
                if (odd == 0) {
                    continue;
                }
                if (odd >= x) {
                    odd -= x;
                    n -= x;
                } else if (odd >= n) {
                    odd -= n;
                    n = 0;
                } else {
                    n -= odd;
                    odd = 0;
                }
            } else {
                if (even == 0) {
                    continue;
                }
                if (even >= x) {
                    even -= x;
                    n -= x;
                } else if (even >= n) {
                    even -= n;
                    n = 0;
                } else {
                    n -= even;
                    even = 0;
                }
            }
        }
        if (n <= 0) out.println("yes");
        else out.println("no");
    }
}
