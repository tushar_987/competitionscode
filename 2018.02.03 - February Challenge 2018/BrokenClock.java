package contest;

import FastIO.InputReader;
import FastIO.OutputWriter;

import java.util.HashMap;
import java.util.Map;

public class BrokenClock {
    private final long MOD = 1000000007;
    Map<Long, Long> saviour;


    public void solve(int testNumber, InputReader in, OutputWriter out) {
        long l = in.nextInt();
        long d = in.nextInt();
        long t = in.nextLong();
        saviour= new HashMap<>();

        // Formula reffered from https://math.stackexchange.com/a/1859464/349064
        //using recurrence relation: cos(nx)=2*cosx*cos(n-1)x - cos(n-2)x;

        //Used Wikipedia for optimizing recurrence.. Link-> https://en.wikipedia.org/wiki/Chebyshev_polynomials
        long constant = (d * invrMod(l)) % MOD; //cos(x)

        if (t == 1) {
            out.println(d);
            return;
        }

        long result = solution(constant,t);
        result = (result * l) % MOD;
        out.println(result < 0 ? result + MOD : result);

    }

    long solution(long cnst ,long t){
        if(t==1)    return cnst;
        if(t%2==0){
            long first;
            if(saviour.containsKey(t/2))  first=saviour.get(t/2);
            else{
                first=solution(cnst,t/2);
                saviour.put(t/2,first);
            }
            long ret=(((2*power(first,2,MOD))%MOD)-1)%MOD;
            return ret;
        }
        else{
            long first,second;
            if(saviour.containsKey(t/2))  first=saviour.get(t/2);
            else    {
                first=solution(cnst,t/2);
                saviour.put(t/2,first);
            }
            if(saviour.containsKey(t/2+1))  second=saviour.get(t/2+1);
            else    {
                second=solution(cnst,t/2 +1);
                saviour.put(t/2+1,second);
            }

            long ret= (((((2*first)%MOD) * second)%MOD) - cnst)%MOD;
            return ret;
        }
    }

    long invrMod(long n) {
        long[] vr = new long[3];
        vr = extended_euclid(n, MOD);
        long x = (vr[1] + MOD) % MOD;
        return x;
    }

    long[] extended_euclid(long a, long b) {
        if (b == 0) {
            long[] arr = new long[3];
            arr[0] = a;
            arr[1] = 1;
            arr[2] = 0;
            return arr;
        } else {
            long[] arr = new long[3];
            long[] arr2 = new long[3];
            arr2 = extended_euclid(b, a % b);
            arr[0] = arr2[0];
            arr[1] = arr2[2];
            long xx = arr2[1];
            arr[2] = (long) (xx - (Math.floor(a / b) * arr2[2]));
            return arr;
        }

    }

    long power(long x, long y, long p) {
        long res = 1;      // Initialize result
        x = x % p;  // Update x if it is more than or equal to p
        while (y > 0) {
            // If y is odd, multiply x with result
            if ((y & 1) == 1)
                res = (res * x) % p;

            // y must be even now
            y = y >> 1; // y = y/2
            x = (x * x) % p;
        }
        return res;
    }

}
