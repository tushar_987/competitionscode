package contest;

import FastIO.InputReader;
import FastIO.OutputWriter;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashSet;

public class ChefAndHisCharacters {
    public void solve(int testNumber, InputReader in, OutputWriter out) {
        char[] ch = in.next().toCharArray();
        if(ch.length<4){
            out.println("normal");
            return;
        }
        Deque<Character> q = new ArrayDeque<>();
        q.addLast(ch[0]);
        q.addLast(ch[1]);
        q.addLast(ch[2]);
        q.addLast(ch[3]);
        long count = 0;
        if (q.contains('c') && q.contains('h') && q.contains('e') && q.contains('f')) count++;
        for (int i = 4; i < ch.length; i++) {
            q.pollFirst();
            q.addLast(ch[i]);
            if (q.contains('c') && q.contains('h') && q.contains('e') && q.contains('f')) count++;
        }
        out.print(count == 0 ? "normal " : "lovely ");
        if (count != 0) out.println(count);
    }
}
