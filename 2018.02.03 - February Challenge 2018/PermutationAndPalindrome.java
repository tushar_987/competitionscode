package contest;

import FastIO.InputReader;
import FastIO.OutputWriter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PermutationAndPalindrome {
    public void solve(int testNumber, InputReader in, OutputWriter out) {
        char[] ch= in.next().toCharArray();

        List<List<Integer>> indices= new ArrayList<>();
        for (int i = 0; i < 26; i++) {
            indices.add(new ArrayList<>());
        }

        int odd_count=0;

        for (int i = 0; i < ch.length; i++) {
            indices.get(ch[i]-'a').add(i);
            if(indices.get(ch[i]-'a').size()%2==0){
                odd_count--;
            }
            else{
                odd_count++;
            }
        }
//        out.println(odd_count);

        if(ch.length%2==0){
            if(odd_count>0) {
                out.println(-1);
                return;
            }
        }
        else{
            if(odd_count>1){
                out.println(-1);
                return;
            }
        }
        int[] result= new int[ch.length];
        int index=0;
        for(int j=0;j<indices.size();j++){
            List<Integer> l= indices.get(j);
            if(l.size()==0) continue;

            if(l.size()%2!=0)   {
                result[ch.length/2]=l.get(l.size()-1);
            }

            for (int i = 0; i+1 < l.size(); i+=2) {
                result[index]=l.get(i);
                result[ch.length-index-1]=l.get(i+1);
                index++;
            }
        }

        for (int i = 0; i < ch.length; i++) {
            out.print(result[i]+1+" ");
        }
        out.println();



    }
}
