package contest;

import FastIO.InputReader;
import FastIO.OutputWriter;

public class CarpalTunnel {
    public void solve(int testNumber, InputReader in, OutputWriter out) {
        int n= in.nextInt();
        int[] arr= in.nextIntArray(n);
        int c= in.nextInt();
        int d= in.nextInt();
        int s= in.nextInt();

        long maximum=arr[0];
        long wait=arr[0];

        for(int i=1;i<n;i++){
            if(arr[i] >maximum){
                wait+=arr[i]-maximum;
                maximum=arr[i];
            }
        }

        out.println(wait*(c-1));
    }
}
