package contest;

import FastIO.InputReader;
import FastIO.OutputWriter;

public class TaskB {
    public void solve(int testNumber, InputReader in, OutputWriter out) {
        int n= in.nextInt();
        int[] arr=in.nextIntArray(n);

//        long max=0;
//        for (int i = 0; i < n; i++) {
//            if(arr[i]-1== 1000000-arr[i]){
//                out.println(arr[i]-1);
//                return;
//            }
//            max=Math.max(max,Math.min(arr[i]-1,1000000-arr[i]));
////            out.println(max);
//        }
//        out.println(max);

        int[] first= new int[n];
        int[] second= new int[n];

        for (int i = 0; i < n; i++) {
            first[i]=arr[i]-1;
        }
        int sub=0;
        for (int i = 0; i < n; i++) {
            if(arr[i]-1 == 1000000-arr[i]){
                sub=1;
            }
            second[i]= 1000000-arr[i]-sub;
        }
    }
}
