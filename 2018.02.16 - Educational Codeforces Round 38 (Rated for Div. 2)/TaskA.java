package contest;

import FastIO.InputReader;
import FastIO.OutputWriter;

public class TaskA {
    public void solve(int testNumber, InputReader in, OutputWriter out) {
        int n= in.nextInt();
        char[] ch= in.next().toCharArray();
        int prev=-1;
        for (int i = 0; i < n; i++) {
            if(ch[i]=='a' || ch[i]=='e' || ch[i]=='i' || ch[i]=='o' || ch[i]=='u' || ch[i]=='y'){
                if(prev==-1){
                    prev=i;
                    out.print(ch[i]);
                }else if(ch[prev]!='a' && ch[prev]!='e' && ch[prev]!='i' && ch[prev]!='o' && ch[prev]!='u' && ch[prev]!='y'){
                    prev=i;
                    out.print(ch[i]);
                }

            }
            else{
                out.print(ch[i]);
                prev=i;
            }

        }
    }
}
