package contest;

import FastIO.InputReader;
import FastIO.OutputWriter;

public class TaskC {
    public void solve(int testNumber, InputReader in, OutputWriter out) {
        int x = in.nextInt();
        for(int n=1; n==1 || n*n - (n/2)*(n/2) <= x; ++n){
            int denom= n*n - x;
            if(denom <0)    continue;

            int sq= getsqrt(denom);
            if(sq<=0)   continue;

            int k= n/sq;
            if( k > 0 && (n*n - (n/k)*(n/k)) == x){
                out.println(n+" "+k);
                return;
            }
        }

        out.println(-1);
    }

    private int getsqrt(int denom) {
        int ret= (int) Math.sqrt(denom);
        if(ret*ret == denom)    return ret;
        else if((ret+1)*(ret+1) ==denom)    return ret+1;
        else    return -1;
    }
}
