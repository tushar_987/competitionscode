#!/bin/python3

import sys


def expectedAmount(a):
    sum=0
    for i in range(1,len(a)-1):
        sum += a[i]*2 
    sum += a[0]*3
    sum += a[len(a)-1]*3
    if sum%6==0 :
        return str(int(sum/6))
    elif sum%3==0 :
        return (str(int(sum/3))+"/2")
    elif sum%2==0 :
        return str(int(sum/2))+"/3"
    else:
        return str(int(sum))+"/6"

if __name__ == "__main__":
    t = int(input().strip())
    for _ in range(t):
        n = int(input().strip())
        a = (list(map(int, input().strip().split(' '))))
        print(expectedAmount(sorted(a)))
