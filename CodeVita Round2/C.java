//package codevita_round2;


import java.io.*;
import java.math.*;
import java.util.*;



/**
 *
 * @author Tushar 11 Aug, 2017
 */
class C {
    public static void main(String args[]) throws IOException
    {

        //File file= new File("COMPLETE_FILE_PATH");
        //InputStream is = new FileInputStream(file);
        
        
        //InputReader in = new InputReader(is);
        InputReader in = new InputReader(System.in);
        PrintWriter out= new PrintWriter(System.out);
        
        int t=1;
        Task solver=new Task();
        //long start = System.nanoTime();
        solver.solve(t,in,out);
        //long stop = System.nanoTime();
        //System.out.println("Execution time: " + ((stop - start) / 1e+6) + "ms.");
        

        out.flush();
        out.close();


    }

    static class Task {
        long mod = 1000000007;
        private static final double pi= 3.14*4.0/3.0;
        

        public void solve(int testNumber, InputReader in,PrintWriter out) throws IOException{
        
            
            BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
            
            
            int n=Integer.parseInt(br.readLine().trim());
            double[] radii= new double[n];
            double[] vol= new double[n];
            String[] arr= new String[n];
            arr=br.readLine().trim().split(" ");
            for(int i=0;i<n;i++){
                
                    radii[i]= Double.parseDouble(arr[i]);
                
                
            }
            
            double k=Double.parseDouble(br.readLine().trim());
            k=k/100.0;
            
            Arrays.sort(radii);
            
//            for(float num:radii){
//                out.println(num);
//            }
//            
//            out.println("-----=--=-=-=-=-=-=");
            
            for(int i=0;i<n;i++){
                double volume=(pi*radii[i]*radii[i]*radii[i]);
//                out.println(volume+"------");
                vol[i]=volume;
            }
            
            for(int i=1;i<n;i++){
                for(int j=0;j<i;j++){
                    vol[j]-=(vol[j]*k);
                }
            }
            
            
//            out.println("+=+=+=======");
//            for(double num:vol){
//                out.println(num);
//            }
            
            double result=0;
            for(int i=0;i<n;i++) result+=vol[i];
            
//            System.out.print(result);
            
//            String str=Double.toString(result);
//            int index=-1;
//            for(int i=0;i<str.length();i++) if(str.charAt(i)=='.')  index=i;
//            
//            String my="";
//            for(int i=0;i<=index+2;i++) my=my+str.charAt(i);
//            System.out.println(df.format(result));
            
            long tmp=(long)(result*100.0);
//            out.println(tmp+"dfhkjsd");
            double f=tmp/100.0;
            
            out.printf("%.2f",f);





        }
            
        int LOG2(long item){
            int count=0;
            while(item>1){
                item>>=1;count++;
            }
            return count;
        }

        long power(long x, long n) {
            if (n <= 0) {
                return 1;
            }
            long y = power(x, n / 2);

            if ((n & 1) == 1) {
                return (((y * y) % mod) * x) % mod;
            }

            return (y * y) % mod;
        }

        long gcd(long a, long b) {
            a = Math.abs(a);
            b = Math.abs(b);
            return BigInteger.valueOf(a).gcd(BigInteger.valueOf(b)).longValue();
        }
    }
    
    
    static class InputReader {
 
        private InputStream stream;
        private byte[] buf = new byte[8192];
        private int curChar, snumChars;
        private SpaceCharFilter filter;
 
        public InputReader(InputStream stream) {
            this.stream = stream;
        }
 
        public int snext() {
            if (snumChars == -1)
                throw new InputMismatchException();
            if (curChar >= snumChars) {
                curChar = 0;
                try {
                    snumChars = stream.read(buf);
                } catch (IOException e) {
                    throw new InputMismatchException();
                }
                if (snumChars <= 0)
                    return -1;
            }
            return buf[curChar++];
        }
 
        public int nextInt() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            int res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public long nextLong() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            long res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public int[] nextIntArray(int n) {
            int a[] = new int[n];
            for (int i = 0; i < n; i++)
                a[i] = nextInt();
            return a;
        }
 
        public String readString() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            StringBuilder res = new StringBuilder();
            do {
                res.appendCodePoint(c);
                c = snext();
            } while (!isSpaceChar(c));
            return res.toString();
        }

        public String next() {
            return readString();
        }
 
        public boolean isSpaceChar(int c) {
            if (filter != null)
                return filter.isSpaceChar(c);
            return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
        }
 
        public interface SpaceCharFilter {
            public boolean isSpaceChar(int ch);
        }
    }


}
