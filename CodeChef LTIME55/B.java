//package codechef_ltime55;

import java.io.*;
import java.util.*;
import java.math.*;

/**
 *
 * @author Tushar 30 Dec, 2017
 */
class B {

    public static void main(String args[]) {

        //File file= new File("COMPLETE_FILE_PATH");
        //InputStream is = new FileInputStream(file);
        //InputReader in = new InputReader(is);
        InputReader in = new InputReader(System.in);
        PrintWriter out = new PrintWriter(System.out);

        int t = in.nextInt();
        while (t-- > 0) {
            Task solver = new Task();
            //long start = System.nanoTime();
            solver.solve(t, in, out);
            //long stop = System.nanoTime();
            //System.out.println("Execution time: " + ((stop - start) / 1e+6) + "ms.");
        }

        out.flush();
        out.close();

    }

    static class Task {

        long mod = 1000000007;

        public void solve(int testNumber, InputReader in, PrintWriter out) {
            long a = in.nextLong();
            long n = in.nextLong();

            long sumD = sumofdigit(a);

            if (a == 9 || a == 1) {
                out.println(a);
                return;
            }
            if(n==1){
                out.println(sumofdigit(a));
                return ;
            }
            
            long result = sumD;
            List<Long> odd= new ArrayList<>(); 
            while (n > 1) {
                if(n%2!=0)  odd.add(sumD);
                sumD = sumofdigit(sumD * sumD);
//                out.println(n + "-- " + sumD);
                n = n >> 1;

            }
            
            while(!odd.isEmpty())   sumD=sumofdigit(sumD*odd.remove(odd.size()-1));
            out.println(sumD);
        }

        long sumofdigit(long num) {
            long result = 0;
            while (num != 0) {
                result += (num % 10);
                num = num / 10;
            }
            if (result > 9) {
                return sumofdigit(result);
            }
            return result;
        }

        int LOG2(long item) {
            int count = 0;
            while (item > 1) {
                item >>= 1;
                count++;
            }
            return count;
        }

        long power(long x, long n) {
            if (n <= 0) {
                return 1;
            }
            long y = power(x, n / 2);

            if ((n & 1) == 1) {
                return (((y * y) % mod) * x) % mod;
            }

            return (y * y) % mod;
        }

        long gcd(long a, long b) {
            a = Math.abs(a);
            b = Math.abs(b);
            return BigInteger.valueOf(a).gcd(BigInteger.valueOf(b)).longValue();
        }
    }

    static class InputReader {

        private InputStream stream;
        private byte[] buf = new byte[8192];
        private int curChar, snumChars;
        private SpaceCharFilter filter;

        public InputReader(InputStream stream) {
            this.stream = stream;
        }

        public int snext() {
            if (snumChars == -1) {
                throw new InputMismatchException();
            }
            if (curChar >= snumChars) {
                curChar = 0;
                try {
                    snumChars = stream.read(buf);
                } catch (IOException e) {
                    throw new InputMismatchException();
                }
                if (snumChars <= 0) {
                    return -1;
                }
            }
            return buf[curChar++];
        }

        public int nextInt() {
            int c = snext();
            while (isSpaceChar(c)) {
                c = snext();
            }
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            int res = 0;
            do {
                if (c < '0' || c > '9') {
                    throw new InputMismatchException();
                }
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }

        public long nextLong() {
            int c = snext();
            while (isSpaceChar(c)) {
                c = snext();
            }
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            long res = 0;
            do {
                if (c < '0' || c > '9') {
                    throw new InputMismatchException();
                }
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }

        public int[] nextIntArray(int n) {
            int a[] = new int[n];
            for (int i = 0; i < n; i++) {
                a[i] = nextInt();
            }
            return a;
        }

        public String readString() {
            int c = snext();
            while (isSpaceChar(c)) {
                c = snext();
            }
            StringBuilder res = new StringBuilder();
            do {
                res.appendCodePoint(c);
                c = snext();
            } while (!isSpaceChar(c));
            return res.toString();
        }

        public String next() {
            return readString();
        }

        public boolean isSpaceChar(int c) {
            if (filter != null) {
                return filter.isSpaceChar(c);
            }
            return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
        }

        public interface SpaceCharFilter {

            public boolean isSpaceChar(int ch);
        }
    }

}
/*
9
1 1
2 1
3 1
4 1
5 1
6 1
7 1
8 1
9 1*/
