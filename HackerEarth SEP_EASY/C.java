//package sep_easy_hackerearth;


import java.io.*;
import java.util.*;
import java.math.*;

/**
 *
 * @author Tushar 1 Sep, 2017
 */
class C {
    public static void main(String args[])
    {

        //File file= new File("COMPLETE_FILE_PATH");
        //InputStream is = new FileInputStream(file);
        
        
        //InputReader in = new InputReader(is);
        InputReader in = new InputReader(System.in);
        PrintWriter out= new PrintWriter(System.out);
        
        int t=1;
        Task solver=new Task();
        //long start = System.nanoTime();
        solver.solve(t,in,out);
        //long stop = System.nanoTime();
        //System.out.println("Execution time: " + ((stop - start) / 1e+6) + "ms.");
        

        out.flush();
        out.close();


    }

    static class Task {
        long mod = 1000000007;

        public void solve(int testNumber, InputReader in,PrintWriter out){
        
            int n=in.nextInt();
            int[] arr= new int[n];
            
            arr= in.nextIntArray(n);
            int[] front= new int[n/2];
            int[] rear= new int[n/2];
            int index=0;
            for(int i=0;i+1<n;i=i+2){
                front[index++]=arr[i]*arr[i+1];
            }
            index=0;
            for(int i=n-1;i-1>=0;i=i-2){
                rear[index++]=arr[i]*arr[i-1];
            }
            
            int start=0;
            int end=0;
            int sum=0;
            while(start<(n/2) && end<(n/2)){
                if(front[start]>rear[end])  {
                    sum+=front[start++];
                }
                else if(front[start]<rear[end]){
                    sum+=rear[end++];
                }
                else{
                    int start_tmp=start;
                    int end_tmp=end;
                    while(start_tmp<n/2 && end_tmp<n/2 && front[start_tmp]==rear[end_tmp]){
                        start_tmp++;
                        end_tmp++;
                        if(front[start_tmp]>rear[end_tmp])  sum+=front[start++];
                        else if(front[start_tmp]<rear[end_tmp]) sum+=rear[end++];
                    }
                    
                }
            }
            
            if(n%2!=0){
                if(start<n/2) sum+=arr[2*start];
                else    sum+=arr[(n-1)-2*end];
            }
            
            
            out.println(sum);
            
           
            





        }
            
        int LOG2(long item){
            int count=0;
            while(item>1){
                item>>=1;count++;
            }
            return count;
        }

        long power(long x, long n) {
            if (n <= 0) {
                return 1;
            }
            long y = power(x, n / 2);

            if ((n & 1) == 1) {
                return (((y * y) % mod) * x) % mod;
            }

            return (y * y) % mod;
        }

        long gcd(long a, long b) {
            a = Math.abs(a);
            b = Math.abs(b);
            return BigInteger.valueOf(a).gcd(BigInteger.valueOf(b)).longValue();
        }
    }
    
    
    static class InputReader {
 
        private InputStream stream;
        private byte[] buf = new byte[8192];
        private int curChar, snumChars;
        private SpaceCharFilter filter;
 
        public InputReader(InputStream stream) {
            this.stream = stream;
        }
 
        public int snext() {
            if (snumChars == -1)
                throw new InputMismatchException();
            if (curChar >= snumChars) {
                curChar = 0;
                try {
                    snumChars = stream.read(buf);
                } catch (IOException e) {
                    throw new InputMismatchException();
                }
                if (snumChars <= 0)
                    return -1;
            }
            return buf[curChar++];
        }
 
        public int nextInt() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            int res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public long nextLong() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            long res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public int[] nextIntArray(int n) {
            int a[] = new int[n];
            for (int i = 0; i < n; i++)
                a[i] = nextInt();
            return a;
        }
 
        public String readString() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            StringBuilder res = new StringBuilder();
            do {
                res.appendCodePoint(c);
                c = snext();
            } while (!isSpaceChar(c));
            return res.toString();
        }

        public String next() {
            return readString();
        }
 
        public boolean isSpaceChar(int c) {
            if (filter != null)
                return filter.isSpaceChar(c);
            return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
        }
 
        public interface SpaceCharFilter {
            public boolean isSpaceChar(int ch);
        }
    }


}
