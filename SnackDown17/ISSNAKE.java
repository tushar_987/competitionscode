import java.io.*;
import java.util.*;
import java.math.*;

//package snackdown17;

/**
 *
 * @author ghost
 */
class ISSNAKE {

 
    public static void main(String args[])
    {
 
        //File file= new File("COMPLETE_FILE_PATH");
        //InputStream is = new FileInputStream(file);
        
        
        //InputReader in = new InputReader(is);
        InputReader in = new InputReader(System.in);
        PrintWriter out= new PrintWriter(System.out);
        
        int t=in.nextInt();
        while(t-->0){
            Task solver=new Task();
            solver.solve(t,in,out);
        }
        
 
        out.flush();
        out.close();
 
 
    }
 
    static class Task {
        long mod = 1000000007;
        boolean[][] visited1,visited2;
        char[][] ch;
        int n;
 
        public void solve(int testNumber, InputReader in,PrintWriter out){
                n=in.nextInt();
                
                visited1= new boolean[2][n];
                visited2= new boolean[2][n];
                
                String arr1=in.next();
                String arr2=in.next();
                
                ch=new char[2][n];
                ch[0]=arr1.toCharArray();
                ch[1]=arr2.toCharArray();
                
                boolean ans1=true;
                boolean ans2=true;
                
                for(int i=0;i<n;i++){
                    if(ch[0][i]=='#'){
                        dfs(0,i,visited1);
                        break;}
                }
                for(int i=0;i<n;i++){
                     if(ch[1][i]=='#'){
                        dfs(1,i,visited2);
                        break;}
                }
                
                for(int i=0;i<2;i++)
                    for(int j=0;j<n;j++){
                        if(ch[i][j]=='#'){
                            if(visited1[i][j]==false)
                                ans1=false;
                        }
                    }
                for(int i=0;i<2;i++)
                    for(int j=0;j<n;j++){
                        if(ch[i][j]=='#'){
                            if(visited2[i][j]==false)
                                ans2=false;
                        }
                    }
                
                out.println((ans1 || ans2)?"yes":"no");
              
                
        }
        void dfs(int row,int col,boolean[][] visited){
            visited[row][col]=true;
            
            if(row==0){
                if(ch[row+1][col]=='#' && !visited[row+1][col])
                    dfs(row+1,col,visited);
                else if((col-1)>=0 && ch[row][col-1]=='#' &&  !visited[row][col-1])
                    dfs(row,col-1,visited);
                else if((col+1)<n && ch[row][col+1]=='#' && !visited[row][col+1])
                    dfs(row,col+1,visited);
            }
            else{
                if(ch[row-1][col]=='#' && !visited[row-1][col] )
                    dfs(row-1,col,visited);
                else if((col-1)>=0 && ch[row][col-1]=='#' && !visited[row][col-1])
                    dfs(row,col-1,visited);
                else if((col+1)<n && ch[row][col+1]=='#' && !visited[row][col+1])
                    dfs(row,col+1,visited);
            }
        }
            
        int LOG2(long item){
            int count=0;
            while(item>1){
                item>>=1;count++;
            }
            return count;
        }
 
 
        long gcd(long a, long b) {
            a = Math.abs(a);
            b = Math.abs(b);
            return BigInteger.valueOf(a).gcd(BigInteger.valueOf(b)).longValue();
        }
    }
    
    
    static class InputReader {
 
        private InputStream stream;
        private byte[] buf = new byte[8192];
        private int curChar, snumChars;
        private SpaceCharFilter filter;
 
        public InputReader(InputStream stream) {
            this.stream = stream;
        }
 
        public int snext() {
            if (snumChars == -1)
                throw new InputMismatchException();
            if (curChar >= snumChars) {
                curChar = 0;
                try {
                    snumChars = stream.read(buf);
                } catch (IOException e) {
                    throw new InputMismatchException();
                }
                if (snumChars <= 0)
                    return -1;
            }
            return buf[curChar++];
        }
 
        public int nextInt() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            int res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public long nextLong() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            int sgn = 1;
            if (c == '-') {
                sgn = -1;
                c = snext();
            }
            long res = 0;
            do {
                if (c < '0' || c > '9')
                    throw new InputMismatchException();
                res *= 10;
                res += c - '0';
                c = snext();
            } while (!isSpaceChar(c));
            return res * sgn;
        }
 
        public int[] nextIntArray(int n) {
            int a[] = new int[n];
            for (int i = 0; i < n; i++)
                a[i] = nextInt();
            return a;
        }
 
        public String readString() {
            int c = snext();
            while (isSpaceChar(c))
                c = snext();
            StringBuilder res = new StringBuilder();
            do {
                res.appendCodePoint(c);
                c = snext();
            } while (!isSpaceChar(c));
            return res.toString();
        }
 
        public String next() {
            return readString();
        }
 
        public boolean isSpaceChar(int c) {
            if (filter != null)
                return filter.isSpaceChar(c);
            return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == -1;
        }
 
        public interface SpaceCharFilter {
            public boolean isSpaceChar(int ch);
        }
    }

}
